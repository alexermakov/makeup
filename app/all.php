<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
            .good {
                background: #1db51d;
                display: inline-block;
                margin: 4px 0;
                padding: 2px 12px 5px;
                color: #fff;
            }
        </style>
    </head>

    <body>
        <ul>
            <li><a class='good' href="index.php">Home</a></li>
            <li><a class='good' href="lips.php">Губы</a></li>
            <li><a class='good' href="brows.php">Брови</a></li>
            <li><a class='good' href="vitiligo.php">Витилиго</a></li>
            <li><a class='good' href="laser.php">Лазерная Эпиляция</a></li>
            <li><a class='good' href="scars.php">Рубцы и шрамы</a></li>
            <li><a class='good' href="areola.php">Ареола</a></li>
            <li><a class='good' href="lamination.php">Ламинирование</a></li>
            <li><a class='good' href="removal.php">Удаление</a></li>
            <li><a class='good' href="contact.php">Контакты</a></li>
            <li><a class='good' href="specialists.php">Специалисты</a></li>
            <li><a class='good' href="specialist.php">Специалист</a></li>
            <li><a class='good' href="price.php">Цены</a></li>
            <li><a class='good' href="404.php">404</a></li>
        </ul>
    </body>

</html>
