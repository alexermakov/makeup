<!DOCTYPE html>
<html lang="ru-RU">

    <head>
        <title>Ламинирование</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body class='page__service'>
        <?php include 'parts/main/header.php'; ?>



        <section class="main__section main__section--service">


            <div class="single__promo">
                <img class='single__promo__bg' src="images/pages/service/lamination/1.jpg">
                <div class="container">
                    <?= breadcrumbs(['Главная','Ламинирование']);?>

                    <div class="single__promo__wrap">
                        <div class="single__promo__info js-reveal-slideLeft">
                            <h1 class="title_x single__promo__title">Лучшее для ваших Бровей и ресниц</h1>
                            <div class="single__promo__text">
                                <p>Актуально сегодня не&nbsp;наращивать ресницы и&nbsp;делать макияж который заметно, а&nbsp;иметь ухоженный вид бровей и&nbsp;ресниц.</p>
                                <p>Уход это самое важное.</p>
                            </div>
                            <a data-fancybox data-src="#js__modal__call"
                                class="btn__default btn__x btn__y--service js__modal">
                                <span class="btn__y__inner">Получить консультацию</span>
                            </a>
                        </div>

                        <div class="single__promo__image js-reveal-slideRight">
                            <div class="single__promo__image__inner">
                                <img src="images/pages/service/lamination/2.jpg" class="single__promo__image__bg">
                                <div class="single__promo__image__info">
                                    <div class="single__promo__image__icon">
                                        <img src="images/pages/service/lamination/3.svg">
                                    </div>
                                    <div class="single__promo__image__text">Ламинирование</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__tags js_single__tags js-reveal-slideUp">
                <div class="container">
                    <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                    <a href=".js_single_ancor_histroy">Истории</a>
                    <a href=".js_single_ancor_text_1">С какими задачами к нам обращаются</a>
                    <a href=".js_single_ancor_text_2">Ламинирование бровей</a>
                    <a href=".js_single_ancor_text_3">Ламинирование ресниц </a>
                    <a href=".js_single_ancor_price">Цены</a>
                </div>
            </div>


            <div class="single__results js_single_ancor_result js-reveal-slideUp">
                <div class="container">
                    <div class="single__results__title">Результаты наших клиентов</div>
                    <div class="single__results__btns js_single__results__btns">
                        <a href="" class='active'>Галерея</a>
                        <a href="">Видео</a>
                    </div>
                    <div class="single__results__list js_single__results__list">
                        <div class="single__results__block active">
                            <div class="single__results__slider js_single__results__slider">
                                <?php for ($i=0; $i < 9; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <div class="single__results__item__col">
                                                <div class="single__results__item__col__image">
                                                    <img src="images/pages/service/lamination/result/<?= $i + 1;?>-0.jpg">
                                                </div>
                                                <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                            </div>
                                            <div class="single__results__item__col">
                                                <div class="single__results__item__col__image">
                                                    <img src="images/pages/service/lamination/result/<?= $i + 1;?>-1.jpg">
                                                </div>
                                                <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                        <div class="single__results__block">
                            <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                                <?php for ($i=0; $i < 7; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                                <img src="images/pages/service/lips/video/1.jpg">
                                            </a>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__history js_single_ancor_histroy js-reveal-slideUp">
                <div class="container">
                    <div class="single__history__top">
                        <div class="single__history__title">Истории</div>
                        <div class="single__history__arrows js_single__history__arrows">
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>

                    <div class="single__history__list js_single__history__list">

                        <?php for ($i=0; $i < 5; $i++):?>
                            <div class="single__history__item__wrap">
                                <div class="single__history__item">
                                    <div class="single__history__images">
                                        <div class="single__history__image">
                                            <img src="images/pages/service/lamination/history/<?= $i % 3 + 1;?>-0.jpg">
                                            <div class="single__history__image__text">До</div>
                                        </div>
                                        <div class="single__history__image">
                                            <img src="images/pages/service/lamination/history/<?= $i % 3 + 1;?>-1.jpg">
                                            <div class="single__history__image__text">После</div>
                                        </div>
                                    </div>
                                    <div class="single__history__info">
                                        <div class="single__history__info__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                                        <div class="single__history__info__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mauris cras ultrices ut sed nec nec, lacus, in. In tellus ut eget leo elit. Neque diam dictum quis et vehicula sit leo fermentum imperdiet.</div>
                                        <div class="single__history__info__who">
                                            <div class="single__history__info__who__image">
                                                <img src="images/__content/about.jpg">
                                            </div>
                                            <div class="single__history__info__who__title">
                                                <div class="single__history__info__who__value">Мастер:</div>
                                                <div class="single__history__info__who__name">Имя Фамилия</div>
                                            </div>
                                        </div>

                                        <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                                    </div>
                                </div>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>

            <div class="single__lamination__text__wrap">
                <div class="single__lamination__row">
                    <div class="container">
                        <div class="single__lamination__info js-reveal-slideUpDelay" data-delay='100'>
                            <div class="single__lamination__title">Ламинирование бровей</div>
                            <div class="single__lamination__text">
                                <p>Долговременная укладка Бровей позволяет сделать так, что их новая форма натуральная женская и широкая</p>
                                <p>Ламинирование позволит сделать брови гуще, каждый волосок напитанный кератином и ботоксом</p>
                                <p>Процедура занимает всего час, результат сохраняется на 3 месяца</p>

                                <p><b>Процедура состоит из нескольких этапов:</b></p>
                                <ul>
                                    <li>Специальный состав в помощь для укладки бровей.</li>
                                    <li>Питание бровей полезным кератином, который делает волоски бровей густыми.</li>
                                    <li>Питание бровей ботоксом, который даёт особый вид.</li>
                                    <li>Окрашивание бровей по оттенку.</li>
                                    <li>Укладка бровей маслом в течении суток позволит сохранить форму на 3 месяца.</li>
                                    <li>В подарок мы подберем для вас воск или гель для укладки, благодаря ему, утренняя укладка формы, должна занять всего лишь 1 минуту вашего времени.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="single__lamination__image js-reveal-slideUpDelay" data-delay='300'>
                            <div class="single__lamination__image__inner">
                                <img src="images/pages/service/lamination/text/1.jpg" alt="">
                            </div>
                        </div>
                        <div class="single__lamination__add__text js-reveal-slideUpDelay" data-delay='100'>
                            <div class="single__lamination__add__text__inner">Подходит всем типам Бровей, прекрасно сочетается с&nbsp;перманентным макияжем , не&nbsp;имеет противопоказаний, позволяет отказаться от косметики навсегда.</div>
                        </div>
                    </div>
                </div>

                <div class="single__lamination__row single__lamination__row--reverse js-reveal-slideUpDelay" data-delay='300'>
                    <div class="container">
                        <div class="single__lamination__info">
                            <div class="single__lamination__title">Ламинирование ресниц </div>
                            <div class="single__lamination__text">
                                <p>Данная процедура обычно может поднять абсолютно все ресницы от места где расположено их основание. Валик которым производится завивка требует индивидуальный подбор - от первичной длины ресниц и по желанию изгиба.</p>

                                <p>Ресницы от данной процедуры должны укрепится и не требовать ухода. Как выглядят ресницы после ? Красиво и&nbsp;лучше всех ! Объемными и густыми, что позволяет максимально отказаться от&nbsp;косметики.</p>

                                <p>Процедура займёт всего час вашего времени, а&nbsp;результат сохранится на&nbsp;три месяца. Больше никаких наращивания ресниц и&nbsp;косметики в&nbsp;течении дня.</p>

                                <p>Данная процедура состоит из нескольких этапов:</p>
                                <ul>
                                    <li>Завивка ресниц с помощью специального состава.</li>
                                    <li>Питание ресниц уходом из кератина и ботокса, которые делают ресницы толще плюс сохраняется длина.</li>
                                    <li>Окрашивание в желаемый цвет и тон: черный или коричневый. 50 оттенков.</li>
                                    <li>Укладка ресниц маслом в течение суток фиксирует форму плюс точно способна помочь сохранить ее на 3 месяца. Этосрок обновления ресниц.</li>
                                    <li>Плюс подарок - масло, цель которого рост, сияние и укладка. Всего за минуту оно создает ухоженный вид ваших ресниц.</li>
                                </ul>
                                <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal btn__lamination_1">Получить консультацию</a>
                            </div>
                        </div>
                        <div class="single__lamination__image js-reveal-slideUpDelay" data-delay='100'>
                            <div class="single__lamination__image__inner">
                                <img src="images/pages/service/lamination/text/2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__price js_single_ancor_price">
                <div class="container">
                    <div class="single__price__title">Цены</div>
                    <div class="price__table">
                        <?php $info = [
                            ['Ламинирование ресниц + ботокс',['3 000 ₽'],''],
                            ['Ламинирование бровей + ботокс',['3 000 ₽'],''],
                            ['Коррекция формы бровей',['600 ₽'],''],
                            ['Окрашивание бровей краской',['400 ₽'],''],
                            ['Окрашивание бровей хной',['600 ₽'],''],
                            ['Окрашивание ресниц краской',['500 ₽'],''],
                        ];?>
                        <?php foreach ($info as $item):?>
                            <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                            <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                                <div class="price__tr__inner">
                                    <div class="price__td price__td--title"><?= $item[0];?></div>
                                    <?php if (count($item[1])>0):?>
                                        <div class="price__td price__td--val">
                                            <?php for ($i=0; $i < count($item[1]); $i++):?>
                                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                            <?php endfor;?>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="btn__y__wrap js-reveal-slideUp">
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>
                </div>
            </div>




            <?php include 'parts/components/another__service.php'; ?>

        </section>


        <?php include 'parts/main/footer.php'; ?>

    </body>
</html>