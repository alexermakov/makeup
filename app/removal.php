<!DOCTYPE html>
<html lang="ru-RU">

    <head>
        <title>Удаление</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body class='page__service'>
        <?php include 'parts/main/header.php'; ?>



        <section class="main__section main__section--service">


            <div class="single__promo">
                <img class='single__promo__bg' src="images/pages/service/removal/1.jpg">
                <div class="container">
                    <?= breadcrumbs(['Главная','Удаление']);?>

                    <div class="single__promo__wrap">
                        <div class="single__promo__info js-reveal-slideLeft">
                            <h1 class="title_x single__promo__title">Удаление <br>перманентного <br>макияжа</h1>
                            <a data-fancybox data-src="#js__modal__call"
                                class="btn__default btn__x btn__y--service js__modal">
                                <span class="btn__y__inner">Получить консультацию</span>
                            </a>
                        </div>

                        <div class="single__promo__image js-reveal-slideRight">
                            <div class="single__promo__image__inner">
                                <img src="images/pages/service/removal/2.jpg" class="single__promo__image__bg">
                                <div class="single__promo__image__info">
                                    <div class="single__promo__image__icon">
                                        <img src="images/pages/service/removal/3.svg">
                                    </div>
                                    <div class="single__promo__image__text">Удаление</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__tags js_single__tags js-reveal-slideUp">
                <div class="container">
                    <a href=".js_single_ancor_what_is">Удалить перманентный макияж </a>
                    <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                    <a href=".js_single_ancor_histroy">Истории</a>
                    <a href=".js_single_ancor_text_2">Что такое лазерное удаление?</a>
                    <a href=".js_single_ancor_text_3">Как удаляет ремувер?</a>
                    <a href=".js_single_ancor_price">Цены</a>
                </div>
            </div>

            <!-- text -->

            <div class="js_single_ancor_what_is removal__text_1">
                <div class="container">
                    <div class="removal__text_1__info js-reveal-slideUpDelay" data-delay='100'>
                        <div class="removal__text_1__title">Удалить перманентный макияж</div>
                        <div class="removal__text_1__text">
                            <p><b></b>Существует несколько методов для удаления остатка перманентного макияжа из кожи:</p>
                            <ul>
                                <li>«Время» — «чудо», «жди меня». Такие работы портят внешность, но девушки их не удаляют, предпочитая верить что со временем наладится бесплатно в хорошем качестве.</li>

                                <li>«Удаление ремувером» — процедура как перманентный макияж, но с эффектом наоборот. Безопасный химический состав наносится на зону перманента и разрушает цвет, ремувер позволяет удалить любой оттенок пигментов. Он проникает в кожу. Цель — проглотить пигмент. После такой процедуры нужен уход, лечащий кожу и способный помочь вывести цвет. Повторная процедура через 2-3 месяца.</li>

                                <li>Лазерное удаление — испарение пигментов из кожи, под действием лазера, без нарушения поверхности кожи и без реабилитации после, с первой же процедуры мы можем убрать почти всю плотность пигмента. Проценты = 70. Лазер видит чёрный, синий и красный оттенки.
                                    <br><br>
                                Повторная процедура через месяц после заживления.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="removal__text_1__image js-reveal-slideUpDelay" data-delay='300'>
                        <img src="images/pages/service/removal/text/0.png">
                    </div>
                </div>
            </div>

            <div class="single__results js_single_ancor_result js-reveal-slideUp">
                <div class="container">
                    <div class="single__results__title">Результаты наших клиентов</div>
                    <div class="single__results__btns js_single__results__btns">
                        <a href="" class='active'>Галерея</a>
                        <a href="">Видео</a>
                    </div>
                    <div class="single__results__list js_single__results__list">
                        <div class="single__results__block active">
                            <div class="single__results__slider js_single__results__slider">
                                <?php for ($i=0; $i < 10; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <div class="single__results__item__col">
                                                <div class="single__results__item__col__image">
                                                    <img src="images/pages/service/removal/result/<?= $i + 1;?>-0.jpg">
                                                </div>
                                                <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                            </div>
                                            <div class="single__results__item__col">
                                                <div class="single__results__item__col__image">
                                                    <img src="images/pages/service/removal/result/<?= $i + 1;?>-1.jpg">
                                                </div>
                                                <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                        <div class="single__results__block">
                            <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                                <?php for ($i=0; $i < 7; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                                <img src="images/pages/service/lips/video/1.jpg">
                                            </a>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__history js_single_ancor_histroy js-reveal-slideUp">
                <div class="container">
                    <div class="single__history__top">
                        <div class="single__history__title">Истории</div>
                        <div class="single__history__arrows js_single__history__arrows">
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>

                    <div class="single__history__list js_single__history__list">

                        <?php for ($i=0; $i < 5; $i++):?>
                            <div class="single__history__item__wrap">
                                <div class="single__history__item">
                                    <div class="single__history__images">
                                        <div class="single__history__image">
                                            <img src="images/pages/service/removal/history/<?= $i % 3 + 1;?>-0.jpg">
                                            <div class="single__history__image__text">До</div>
                                        </div>
                                        <div class="single__history__image">
                                            <img src="images/pages/service/removal/history/<?= $i % 3 + 1;?>-1.jpg">
                                            <div class="single__history__image__text">После</div>
                                        </div>
                                    </div>
                                    <div class="single__history__info">
                                        <div class="single__history__info__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                                        <div class="single__history__info__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mauris cras ultrices ut sed nec nec, lacus, in. In tellus ut eget leo elit. Neque diam dictum quis et vehicula sit leo fermentum imperdiet.</div>
                                        <div class="single__history__info__who">
                                            <div class="single__history__info__who__image">
                                                <img src="images/__content/about.jpg">
                                            </div>
                                            <div class="single__history__info__who__title">
                                                <div class="single__history__info__who__value">Мастер:</div>
                                                <div class="single__history__info__who__name">Имя Фамилия</div>
                                            </div>
                                        </div>

                                        <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                                    </div>
                                </div>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>

            <!-- text -->

            <div class="removal__text_2 js-reveal-slideUpDelay" data-delay='00'>
                <div class="removal__text_2__1 js_single_ancor_text_2">
                    <div class="container">
                        <div class="removal__text_2__1__title">
                            <svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="37.5" cy="37.5" r="37.5" fill="#534859"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M34.4292 16.1744C31.9721 16.6623 29.5791 17.937 27.7861 19.7134C25.9352 21.5469 24.6624 23.9493 24.1602 26.5568C23.9503 27.6471 23.9458 29.5767 24.1517 30.236C24.4878 31.3124 25.5258 32.3911 26.6222 32.8035C27.3863 33.0909 28.4692 33.1321 29.231 32.903C31.0261 32.3628 32.2156 30.7939 32.2156 28.9667C32.2156 27.5927 32.6809 26.4662 33.6417 25.5144C34.8855 24.2822 36.4792 23.845 38.1673 24.2726C39.0441 24.4948 39.7001 24.8699 40.3462 25.5187C41.275 26.4514 41.7268 27.5623 41.7142 28.8828C41.6984 30.5288 40.9255 31.8957 39.491 32.8142C38.7341 33.2989 38.0186 33.4923 36.7809 33.5467C35.111 33.6202 34.2668 33.9328 33.4189 34.7912C32.879 35.3377 32.4664 36.0991 32.3117 36.834C32.2501 37.127 32.2156 38.6405 32.2156 41.0587C32.2156 44.1389 32.2395 44.9179 32.3468 45.3255C32.8721 47.3235 34.5857 48.5829 36.607 48.4565C37.8464 48.379 38.9346 47.7986 39.6529 46.8319C40.3641 45.8749 40.422 45.621 40.4583 43.3028L40.4902 41.2688L41.2008 41.0326C41.5917 40.9027 42.295 40.6073 42.7638 40.3762C48.9737 37.3146 51.6794 29.9826 48.9229 23.6865C47.2399 19.8422 43.7057 17.0211 39.5121 16.1744C38.3598 15.9419 35.6009 15.9419 34.4292 16.1744ZM35.2654 50.1656C34.2821 50.3768 33.489 50.8161 32.7437 51.5623C31.3798 52.9278 30.9289 54.7643 31.5105 56.5852C32.413 59.4108 35.7436 60.8122 38.4617 59.51C41.0444 58.2726 42.0547 55.1915 40.708 52.6593C40.2993 51.8909 39.3578 50.9885 38.5635 50.6038C37.4544 50.0668 36.3861 49.9248 35.2654 50.1656Z" fill="white"/>
                            </svg>
                            <div class="removal__text_2__1__title__inner">Что такое лазерное удаление?</div>
                        </div>

                        <div class="removal__text_2__1__info">
                            <div class="removal__text_2__1__image js-reveal-slideUpDelay" data-delay='300'>
                                <img src="images/pages/service/removal/text/1.png" alt="">
                            </div>
                            <div class="removal__text_2__1__text js-reveal-slideUpDelay" data-delay='100'>
                                <ul>
                                    <li>Мы должны идти специальной насадкой лазерного аппарата, по зоне перманентного макияжа, пигмент мгновенно светлеет и она меняет оттенок. Это помогает вывести цвет.</li>

                                    <li>Лазерное удаление — лучший метод удаления в мире.</li>
                                    <li>Процедура одна или несколько с использованием различных насадок.</li>
                                    <li>Процесс на минуту.</li>

                                    <li>Проводим охлаждение за счёт крема, затем проводим лазерное удаление плюс обработка Пантенол.</li>

                                    <li>Противопоказания у метода «лазерное удаление» существуют, поэтому лучше всего перед самой процедурой подписанный договор.</li>

                                    <li>Лазерное удаление прекрасно поможет вывести любые темные оттенки: черные, синие и красные тона. Волна луча работает в определенном диапазоне, и светлые оттенки попросту не увидит. </li>

                                    <li>Именно поэтому для светлых оттенков мы используем удаление ремувером.</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="removal__text_2__form js-reveal-slideUpDelay" data-delay='0'>
                    <div class="container">
                        <div class="removal__text_2__form__info js-reveal-slideUpDelay" data-delay='300'>
                            <p>Перед процедурой необходима консультация.  Метод и процесс подразумевают не приятные ощущения.</p>
                            <p>Допустимы ощущения: отёк, покраснение, тепло. Ощущения проходят в течение часа. </p>
                            <p>Время взять консультацию, чтобы избавиться  от чужих ошибок на вашем лице.</p>
                        </div>
                        <div class="removal__text_2__form__wrap js-reveal-slideUpDelay" data-delay='400'>
                            <div class="removal__text_2__form__title">Приглашаю вас на консультацию, чтобы избавиться от чужих ошибок на вашем лице </div>
                            <form action="javascript:void(0)" class='js_form'>
                                <div class="form__field">
                                    <input type="text" name="name" required placeholder="Имя">
                                </div>

                                <div class="form__field">
                                    <input type="tel" name="phone" required  class='js__phone__mask' placeholder="+7 9__ ___ ____">
                                </div>

                                <div class="form__field form__field--btn">
                                    <button class="btn__default btn__x btn__modal__call">Получить консультацию</button>
                                </div>
                                <div class="form__field form__field--agree">
                                    <label>
                                        <input required type="checkbox" name="agree">
                                        <div class="form__field--agree__view"></div>
                                        <div class="form__field--agree__text">Я выражаю согласие на обработку своих персональных данных в соответствии с политикой</div>
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="removal__text_3__1 js_single_ancor_text_3">
                    <div class="container">


                        <div class="removal__text_3__1__info js-reveal-slideUpDelay" data-delay='100'>
                            <div class="removal__text_3__1__title">Как удаляет ремувер?</div>
                            <div class="removal__text_3__1__text">
                                <ul>
                                    <li>Ремувер это специальный химический состав, которым мы работаем.</li>

                                    <li>Мы работаем с помощью кислотного ремувера, потому что он самый мягкий и безопасный.</li>

                                    <li>Цвет не вносится в кожу, а при помощи ремувера выводится на поверхность. После процедуры период нельзя наносить косметику. Специальным стерильным картриджем, вводим в кожу ремувер и вместе со старым пигментом идет вывод к поверхности в виде шелушений.</li>

                                    <li>Для того чтобы понять, каким методом лучше удалять, рекомендуем предоставить фотографию плюс чтобы вы прибыли на консультацию.</li>

                                    <li>Удаление ремувером иногда необходимо проводить несколько раз.</li>

                                    <li>Важно между процедурами необходимо наносить средства для ухода.</li>

                                    <li>Удаление старого перманентного макияжа, опыт позволит нам провести процедуру качественно, сохранить ваше качество кожи, без рубцов и без боли.</li>

                                    <li>Время процедуры — час. Результат распада цвета произойдёт сразу и далее в течении 2 месяцев пигмент будет продолжать уходить из кожи — затем повторная процедура.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="removal__text_3__1__image js-reveal-slideUpDelay" data-delay='300'>
                            <div class="removal__text_3__1__image__inner">
                                <img src="images/pages/service/removal/text/2.jpg">
                            </div>
                        </div>

                    </div>
                </div>

            </div>




            <div class="single__price js_single_ancor_price">
                <div class="container">
                    <div class="single__price__title">Цены</div>
                    <div class="price__table">
                        <?php $info = [
                            ['Лазерное удаление перманентного макияжа',['5 000 ₽'],''],
                            ['Лазерное удаление татуировки',['от 2 000 ₽'],''],
                            ['Удаление ремувером',[''],''],
                        ];?>
                        <?php foreach ($info as $item):?>
                            <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                            <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                                <div class="price__tr__inner">
                                    <div class="price__td price__td--title"><?= $item[0];?></div>
                                    <?php if (count($item[1])>0):?>
                                        <div class="price__td price__td--val">
                                            <?php for ($i=0; $i < count($item[1]); $i++):?>
                                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                            <?php endfor;?>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="btn__y__wrap js-reveal-slideUp">
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>
                </div>
            </div>


            <?php include 'parts/components/another__service.php'; ?>

        </section>


        <?php include 'parts/main/footer.php'; ?>

    </body>
</html>