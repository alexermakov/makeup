<!-- start footer -->

<footer class="main_footer">
    <div class="container">

        <div class="main_footer__top">
            <div class="main_footer__menu main_footer__col--1 js-reveal-slideUp">
                <div class="main_footer__top__title">Компания</div>
                <ul class="main_footer__list">
                    <li><a href="specialists.php">Специалисты</a></li>
                    <li><a href="price.php">Цены</a></li>
                    <li><a href="contact.php">Контакты</a></li>
                </ul>
            </div>
            <div class="main_footer__menu main_footer__col--2 js-reveal-slideUp">
                <div class="main_footer__top__title">Наши услуги</div>
                <ul class="main_footer__list">
                    <li><a href="brows.php">Брови</a></li>
                    <li><a href="lips.php">Губы</a></li>
                    <li><a href="vitiligo.php">Витилиго </a></li>
                    <li><a href="scars.php">Шрамы/ рубцы </a></li>
                </ul>
            </div>
            <div class="main_footer__menu main_footer__col--3 js-reveal-slideUp">
                <div class="main_footer__top__title">Наши услуги</div>
                <ul class="main_footer__list">
                    <li><a href="areola.php">Ареолы </a></li>
                    <li><a href="laser.php">Лазерная эпиляция </a></li>
                    <li><a href="removal.php">Удаление перманета </a></li>
                    <li><a href="lamination.php">Ламинирование</a></li>
                </ul>
            </div>
            <div class="main_footer__contact main_footer__col--4 js-reveal-slideUp">
                <div class="main_footer__top__title"><a href="contact.php">Адрес</a></div>
                <div class="main_footer__contact__list">
                    <div class="main_footer__contact__item">
                        <svg width="22" height="28" viewBox="0 0 22 28" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M11 0C8.08369 0.00344047 5.28779 1.16347 3.22564 3.22563C1.16348 5.28778 0.00345217 8.08367 1.17029e-05 11C-0.00348119 13.3832 0.774992 15.7018 2.21601 17.6C2.21601 17.6 2.51601 17.995 2.56501 18.052L11 28L19.439 18.047C19.483 17.994 19.784 17.6 19.784 17.6L19.785 17.597C21.2253 15.6996 22.0034 13.3821 22 11C21.9966 8.08367 20.8365 5.28778 18.7744 3.22563C16.7122 1.16347 13.9163 0.00344047 11 0ZM7.00013 11.0304C6.99996 11.0089 6.99997 10.9875 7.00015 10.966C7.00915 9.88424 7.44758 8.90483 8.15333 8.18995C8.15939 8.1838 8.16548 8.17768 8.17158 8.17157C8.19344 8.14972 8.21552 8.12814 8.23784 8.10684C8.94827 7.42837 9.90821 7.00894 10.9661 7.00014C10.9875 6.99996 11.009 6.99995 11.0304 7.00011C13.2255 7.01645 15 8.801 15 11C15 13.2091 13.2092 15 11 15C8.80101 15 7.01646 13.2255 7.00013 11.0304Z" fill="#F9698D"/></svg>
                        <div class="main_footer__contact__item__value">
                            <b>г. Москва</b>
                            Улица Кастанаевская д 55 кор 1 Метро Славянский бульвар
                        </div>
                    </div>

                    <div class="main_footer__contact__item">
                        <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.3343 16.3459L17.3148 14.3646C17.5816 14.1011 17.9191 13.9207 18.2863 13.8453C18.6536 13.7699 19.0349 13.8029 19.3838 13.9401L21.7975 14.9042C22.1501 15.0474 22.4525 15.2918 22.6665 15.6066C22.8805 15.9214 22.9965 16.2925 23 16.6732V21.0958C22.9979 21.3548 22.9435 21.6107 22.84 21.848C22.7365 22.0854 22.5861 22.2994 22.3977 22.477C22.2094 22.6547 21.9871 22.7924 21.7441 22.8819C21.5012 22.9713 21.2426 23.0107 20.9841 22.9975C4.07008 21.945 0.657214 7.61574 0.0117748 2.13173C-0.0181869 1.86242 0.00918831 1.58982 0.0920994 1.33186C0.17501 1.07391 0.311578 0.836431 0.492819 0.635064C0.674059 0.433698 0.895864 0.273003 1.14364 0.163552C1.39142 0.0541004 1.65956 -0.00162677 1.93041 3.61494e-05H6.20091C6.582 0.00116461 6.95403 0.116322 7.26915 0.330695C7.58428 0.545069 7.8281 0.848856 7.96924 1.20298L8.93297 3.61772C9.07467 3.9654 9.11081 4.34713 9.03691 4.71524C8.963 5.08335 8.78231 5.42151 8.51742 5.68749L6.53689 7.66882C6.53689 7.66882 7.67746 15.3907 15.3343 16.3459Z" fill="#F9698D"/></svg>
                        <div class="main_footer__contact__item__value">
                            <a href="tel:+7 (926) 762 61 07">+7 (926) 762 61 07</a>
                        </div>
                    </div>

                    <div class="main_footer__contact__item">
                        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.5 25C5.5957 25 0 19.4043 0 12.5C0 5.5957 5.5957 0 12.5 0C19.4043 0 25 5.5957 25 12.5C25 19.4043 19.4043 25 12.5 25ZM11.3281 12.5C11.3281 12.8906 11.5234 13.2568 11.8506 13.4326L16.5381 16.5576C17.0752 16.958 17.8027 16.8115 18.1201 16.2744C18.5205 15.7373 18.374 15.0098 17.8369 14.6484L13.6719 11.875V5.85938C13.6719 5.20996 13.1494 4.6875 12.4561 4.6875C11.8506 4.6875 11.2842 5.20996 11.2842 5.85938L11.3281 12.5Z" fill="#F9698D"/></svg>
                        <div class="main_footer__contact__item__value">
                            10:00 - 22:00 Ежедневно
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="main_footer__bottom js-reveal-slideUp">
            <div class="main_footer__copyright">
                <a href="index.php" class="main_footer__logo">
                    <img src="images/logo.svg" alt="footer logo">
                </a>
                <div class="main_footer__copyright__text">
                    © Без макияжа, 2022 <br> Все права защищены
                </div>
            </div>

            <div class="main_footer__bottom__links">
                <a href="">Политика конфиденциальности</a>
                <a href="">Договор оферты</a>
            </div>


            <div class="main_footer__social">
                <a href="" target="_blank">
                    <img src="images/icons/social/you.svg">
                </a>
                <a href="" target="_blank">
                    <img src="images/icons/social/vk.svg">
                </a>
            </div>
        </div>

    </div>
</footer>


<?php include 'parts/main/modal.php'; ?>
<script src='https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=5e33ee61-cce3-40b3-a20a-bbce70a3f698'></script>
<script src="js/app.min.js?<?= time();?>"></script>

