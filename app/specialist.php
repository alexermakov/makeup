<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Специалисты</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body>
        <?php include 'parts/main/header.php'; ?>

        <section class="main__section main__section--specialist">
            <div class="container">
                <?= breadcrumbs(['Главная','Мастера', 'Кусакина Татьяна Дмитриевна']);?>
                <h1 class="title_x">Кусакина Татьяна Дмитриевна</h1>

                <div class="specialist__top">
                    <div class="specialist__top__image js-reveal-slideLeft">
                        <div class="specialist__top__image__inner">
                            <img src="images/__content/about.jpg">
                        </div>
                    </div>
                    <div class="specialist__top__info js-reveal-slideRight">
                        <div class="specialist__top__info__item">
                            <div class="specialist__top__info__item__title">Специальность:</div>
                            <div class="specialist__top__info__item__text">Главный мастер Студии на Кутузовском проспекте</div>
                        </div>
                        <div class="specialist__top__info__item">
                            <div class="specialist__top__info__item__title">Опыт работы:</div>
                            <div class="specialist__top__info__item__text">15 лет</div>
                        </div>
                        <div class="specialist__top__info__item">
                            <div class="specialist__top__info__item__title">Образование:</div>
                            <div class="specialist__top__info__item__text">Кафедра управления Медицинским Учреждением НИИ Москвы ГУПХГБУ</div>
                        </div>
                    </div>
                </div>

                <div class="specialist__text js-reveal-slideUp">
                    <ul>
                        <li>Основными направлениями работы Татьяны Дмитриевны являются колористика, геометрия и симметрия, последовательное и послойное внесение пигментов в кожу.</li>

                        <li>Является специалистом с многолетним опытом по исправлению перманентного макияжа, удалению некачественного перманентного макияжа, камуфляжу рубцов, анестезированию перед процедурами травмирующими кожу.</li>

                        <li>Владеет всеми методами Перманентного Макияжа.</li>

                        <li> Действительный член «Ассоциации Перманентного Макияжа России».</li>

                        <li>Также Татьяна Дмитриевна является сертифицированным специалистом большинства топ брендов производителей аппаратов для перманентного макияжа: NPM ДОПИСАТЬ</li>

                        <li> Кроме того, одним из первых в России начала активно посещать Академии Перманентного Макияжа по всему миру, построенные производителями: NPM (Израиль), ДОПИСАТЬ </li>

                        <li>С 2014 по 2018 год занимала пост главного Мастера Перманентного Макияжа,в клинике “Lique Deluxe” на Таганской.</li>

                        <li>Опыт работы 15 лет.</li>
                    </ul>
                </div>

                <div class="specialist__title js-reveal-slideUp">
                    <img src="images/icons/specialist/1.svg">
                    <div class="specialist__title__text">Повышение квалификации</div>
                </div>

                <div class="specialist__text js-reveal-slideUp">
                    <ul>
                        <li>2007г. Первичная переподготовка по программе «Ультразвуковая диагностика». Факультет усовершенствования врачей МОНИКИ 576 часов. 2018г – сертификационный курс УЗД.</li>

                        <li>2010, 2015 гг. Сертификационный курс. Специальность – хирургия. Первый Московский государственный медицинский университет имени И.М.Сеченова Министерства Здравоохранения и социального развития Российской Федерации.</li>

                        <li>2013, 2018г – Диплом и сертификат о профессиональной переподготовке «Организация Здравоохранения и общественное здоровье».</li>

                        <li>2007г. Первичная переподготовка по программе «Ультразвуковая диагностика». Факультет усовершенствования врачей МОНИКИ 576 часов. 2018г – сертификационный курс УЗД.</li>

                        <li>2010, 2015 гг. Сертификационный курс. Специальность – хирургия. Первый Московский государственный медицинский университет имени И.М.Сеченова Министерства Здравоохранения и социального развития Российской Федерации.</li>

                        <li>2013, 2018г – Диплом и сертификат о профессиональной переподготовке «Организация Здравоохранения и общественное здоровье».</li>

                        <li> 2007г. Первичная переподготовка по программе «Ультразвуковая диагностика». Факультет усовершенствования врачей МОНИКИ 576 часов. 2018г – сертификационный курс УЗД.</li>

                        <li>2010, 2015 гг. Сертификационный курс. Специальность – хирургия. Первый Московский государственный медицинский университет имени И.М.Сеченова Министерства Здравоохранения и социального развития Российской Федерации.</li>

                        <li>2013, 2018г – Диплом и сертификат о профессиональной переподготовке «Организация Здравоохранения и общественное здоровье».</li>

                    </ul>
                </div>

                <div class="specialist__title js-reveal-slideUp">
                    <img src="images/icons/specialist/2.svg">
                    <div class="specialist__title__text">Дипломы и лицензии</div>
                </div>

                <?php $sizes = ['4','4','4','4','3','3','3','4','4','4','4','3','3','3',];?>
                <div class="specialist__doc__list js-reveal-slideUp">
                    <?php for ($i=0; $i < count($sizes); $i++) :?>
                        <div class="specialist__doc specialist__doc--size-<?= $sizes[$i]; ?>">
                            <a data-fancybox='doc' data-src="images/__content/specialist/doc/<?= $i;?>.jpg" class="specialist__doc__image">
                                <img src="images/__content/specialist/doc/<?= $i +1;?>.jpg">
                            </a>
                            <div class="specialist__doc__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                        </div>
                    <?php endfor; ?>

                </div>

                <div class="btn__y__wrap js-reveal-slideUp">
                    <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                        <span class="btn__y__inner">Получить консультацию</span>
                    </a>
                </div>

            </div>
        </section>


        <?php include 'parts/main/footer.php'; ?>

    </body>
</html>