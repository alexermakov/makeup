<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Брови</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body class='page__service'>
        <?php include 'parts/main/header.php'; ?>



        <section class="main__section main__section--service">


            <div class="single__promo">
                <img class='single__promo__bg' src="images/pages/service/brows/1.jpg">
                <div class="container">
                    <?= breadcrumbs(['Главная','Брови']);?>

                    <div class="single__promo__wrap">
                        <div class="single__promo__info js-reveal-slideLeft">
                            <h1 class="title_x single__promo__title">Брови</h1>
                            <div class="single__promo__text">
                                <p>Тысячи преображений и 15 лет опыта помогли нам научиться создавать исключительно натуральные образы.</p>
                                <p>Узнайте какими могут быть <b>ваши брови</b></p>
                            </div>
                            <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x btn__y--service js__modal">
                                <span class="btn__y__inner">Получить консультацию</span>
                            </a>
                        </div>

                        <div class="single__promo__image js-reveal-slideRight">
                            <div class="single__promo__image__inner">
                                <img src="images/pages/service/brows/2.jpg"  class="single__promo__image__bg">
                                <div class="single__promo__image__info">
                                    <div class="single__promo__image__icon">
                                        <img src="images/pages/service/brows/3.svg">
                                    </div>
                                    <div class="single__promo__image__text">Брови</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single__tags js_single__tags js-reveal-slideUp">
                <div class="container">
                    <a href=".js_single_ancor_wantchange">Хотите исправить</a>
                    <a href=".js_single_ancor_yougot">Вы получите как результат</a>
                    <a href=".js_single_ancor_method">Применяемые методы</a>
                    <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                    <a href=".js_single_ancor_histroy">Истории</a>
                    <a href=".js_single_ancor_price">Цены</a>
                </div>
            </div>

            <div class="single__different">
                <div class="container">
                    <div class="single__different__col js_single_ancor_wantchange js-reveal-slideUpDelay" data-delay='100'>
                        <div class="single__different__title">
                            <div class="single__different__title__icon">
                                <svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="37.5" cy="37.5" r="37.5" fill="#534859"/><path d="M33.2218 20.226C33.888 18.5714 34.9986 17.5087 36.4571 17.1297C38.1871 16.6805 40.2067 17.4297 41.1846 18.8834C42.1975 20.3896 42.1944 20.2936 41.5714 31.2585C41.2604 36.731 41.0094 41.6523 41.0135 42.1944C41.0293 44.2428 40.2286 45.6965 38.7332 46.3345C37.8016 46.732 37.0547 46.6743 35.9891 46.1222C34.4244 45.3115 34.2605 44.8359 33.995 40.3407C33.8737 38.286 33.5986 33.6566 33.3836 30.0532C32.9135 22.1688 32.895 21.0375 33.2218 20.226Z" fill="white"/><path d="M33.0916 54.7039C32.9355 52.5827 34.2574 50.6523 36.3465 49.9511C37.0638 49.7104 38.8088 49.8957 39.5713 50.2935C40.3977 50.7249 41.3031 51.7253 41.6736 52.6165C42.0713 53.5732 42.0236 55.5645 41.5818 56.4458C40.76 58.0854 39.3058 59.0062 37.5478 59C35.0855 58.9913 33.2792 57.25 33.0916 54.7039Z" fill="white"/></svg>
                            </div>
                            Хотите исправить:
                        </div>
                        <div class="single__different__text">
                            <p>Опущенные брови и печальный взгляд</p>
                            <p>Брови тонкие и светлые</p>
                            <p>Не имеют формы и густоты</p>
                            <p>Не создают красоту вашему образу</p>
                            <p>Ассиметричные по строению</p>
                            <p>Разные по высоте</p>
                            <p>Шрамы и рубцы в зоне бровей</p>
                            <p>Компенсировать потерю волос после химиотерапии</p>
                            <p>Не стильная форма</p>
                            <p>Каждый день вы их красите по разному</p>
                            <p>Не растут головки бровей</p>
                            <p>Не растут хвостики бровей</p>
                            <p>Устали тратить время и деньги на походы к бровисту</p>
                            <p>У вас старый татуаж и он раздражает</p>
                            <p>Хотите изменит форму и цвет Бровей</p>
                            <p>Хотите по утрам не делать макияж</p>
                            <p>Все сложнее найти подходящий оттенок для Бровей в косметике</p>
                        </div>
                    </div>
                    <div class="single__different__col js_single_ancor_yougot  js-reveal-slideUpDelay" data-delay='300'>
                        <div class="single__different__title">
                            <div class="single__different__title__icon">
                                <svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="37.5" cy="37.5" r="37.5" fill="#FB678D"/><path d="M40.6977 33.6827C47.7551 26.5829 48.9987 25.3873 49.5089 25.2105C50.3994 24.9019 51.3105 24.9335 52.0904 25.2997C53.7569 26.0823 54.4657 28.0491 53.6792 29.7094C53.231 30.6553 34.6912 49.3325 33.8003 49.7355C33.0192 50.0888 31.9388 50.0881 31.1602 49.7339C30.7221 49.5346 29.3932 48.2863 26.0967 44.9778C21.1565 40.0194 20.9971 39.8125 21 38.3621C21.002 37.3349 21.3178 36.6374 22.0911 35.9509C22.7744 35.3444 23.4357 35.0993 24.3773 35.1036C25.5653 35.1089 25.9805 35.4178 29.3453 38.7987L32.4805 41.9491L40.6977 33.6827Z" fill="white"/></svg>
                            </div>
                            Вы получите как результат:
                        </div>
                        <div class="single__different__text">
                            <p>100% симметричные по форме брови</p>
                            <p>В одном оттенке и яркости</p>
                            <p>Любой желаемой формы</p>
                            <p>Без макияжа и без подкрашиваний</p>
                            <p>Держаться 2-3 года</p>
                            <p>100% водостойкие</p>
                            <p>Прекрасная основа под вечерний макияж</p>
                            <p>Открытый и отдохнувший взгляд</p>
                            <p>Экономия времени по утрам</p>
                            <p>Уменьшение расходов на косметику</p>
                            <p>Минус 10 лет с лица</p>
                            <p>Внешность которая повышает самооценку</p>
                            <p>Натуральный эффект без косметики 24/7</p>
                            <p>Современный образ</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__text__1 js_single_ancor_method js-reveal-slideUpDelay" data-delay='100'>
                <div class="container">
                    <div class="single__text__1__info">
                        <h2 class="single__text__1__title">Применяемые методы:</h2>
                        <div class="single__text__1__text">
                            <ol>
                                <li>Метод волоски — самый естественных в мире перманентного макияжа, идеальная имитация волосков на коже, которые не отличаются от натуральных. Именно ими мы научились создавать любую форму, объем и цвет. Подходят практически всем типам лиц и именно этим методом мы работаем чаще всего, потому что как и вы, любим все натуральное.</li>

                                <li>Метод напыления — эффект легкого макияжа в зоне Бровей.  Метод актуален для тех, кто хочет усилить объем и оттенок, но при этом имеет свои брови.</li>

                                <li>Метод волоски + напыление, подходит тем девушкам у которых есть часть своих бровей, которая густая, например головка, но почти отсутствует хвостики Бровей. Мы достраиваем методом волоски сами хвостики или ширину, а под родными волосками создаем тень за счёт напыления, в итоге получается естественные брови, ухоженные и объёмные, без косметики.</li>
                            </ol>
                            <p>Стойкость оттенка зависит от типа кожи, частоты занятий спортом, загара и посещений сауны. В течении двух лет, рисунок светлеет до чистой кожи и не требует удаления лазером или ремувером.</p>
                        </div>
                    </div>
                    <div class="single__text__1__image js-reveal-slideUpDelay" data-delay='300'>
                        <div class="single__text__1__image__inner">
                            <img src="images/pages/service/brows/text/1.jpg">
                        </div>
                    </div>
                </div>
            </div>



            <div class="js-reveal-slideUp single__results js_single_ancor_result">
                <div class="container">
                    <div class="single__results__title">Результаты наших клиентов</div>
                    <div class="single__results__btns js_single__results__btns">
                        <a href="" class='active'>Галерея</a>
                        <a href="">Видео</a>
                    </div>
                    <div class="single__results__list js_single__results__list">
                        <div class="single__results__block active">
                            <div class="single__results__slider js_single__results__slider">
                                <?php for ($i=0; $i < 4; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <div class="single__results__item__col">
                                                <a href="images/pages/service/brows/result/<?= $i + 1;?>-0.jpg" data-fancybox='single__results--<?= $i;?>' class="single__results__item__col__image">
                                                    <img src="images/pages/service/brows/result/<?= $i + 1;?>-0.jpg">
                                                </a>
                                                <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                            </div>
                                            <div class="single__results__item__col">
                                                <a href="images/pages/service/brows/result/<?= $i + 1;?>-1.jpg" data-fancybox='single__results--<?= $i;?>' class="single__results__item__col__image">
                                                    <img src="images/pages/service/brows/result/<?= $i + 1;?>-1.jpg">
                                                </a>
                                                <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                        <div class="single__results__block">
                            <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                                <?php for ($i=0; $i < 7; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                                <img src="images/pages/service/lips/video/1.jpg">
                                            </a>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="js-reveal-slideUp single__history js_single_ancor_histroy">
                <div class="container">
                    <div class="single__history__top">
                        <div class="single__history__title">Истории</div>
                        <div class="single__history__arrows js_single__history__arrows">
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>

                    <div class="single__history__list js_single__history__list">

                        <?php $info = [
                            ['Обидно же, когда вас все время спрашивают, почему вы такие грустные?','Вот и эту девушка расстраивали такие вопросы. На самом деле она очень даже веселая, но из-за того, что у нее были тонкие опущенные брови...'],
                            ['Вижу  возрастные изменения, помогите их убрать !','К нам обратилась дама 40+, и у нее был такой запрос: «Я вижу возрастные изменения и не знаю что с ними делать, так как не хочу прибегать к инъекциям. Скажите, пожалуйста, за счет каких...'],
                            ['Муж был против, но я решилась !','Иногда очень хочется что-то поменять в лице, но боишься реакции близких. Вот и эта девушка обратилась к нам с просьбой сделать такой перманентный макияж, чтобы его не заметил ее муж...'],
                        ];?>
                        <?php for ($i=0; $i < 5; $i++):?>
                            <div class="single__history__item__wrap">
                                <div class="single__history__item">
                                    <div class="single__history__images">
                                        <div class="single__history__image">
                                            <img src="images/pages/service/brows/history/<?=$i % 3 + 1;?>-0.jpg">
                                            <div class="single__history__image__text">До</div>
                                        </div>
                                        <div class="single__history__image">
                                            <img src="images/pages/service/brows/history/<?= $i % 3 + 1;?>-1.jpg">
                                            <div class="single__history__image__text">После</div>
                                        </div>
                                    </div>
                                    <div class="single__history__info">
                                        <div class="single__history__info__title"><?= $info[$i % 3][0];?></div>
                                        <div class="single__history__info__text"><?= $info[$i % 3][1];?>.</div>
                                        <div class="single__history__info__who">
                                            <div class="single__history__info__who__image">
                                                <img src="images/__content/about.jpg">
                                            </div>
                                            <div class="single__history__info__who__title">
                                                <div class="single__history__info__who__value">Мастер:</div>
                                                <div class="single__history__info__who__name">Кусакина Татьяна Дмитриевна</div>
                                            </div>
                                        </div>

                                        <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                                    </div>
                                </div>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>


            <div class="single__price js_single_ancor_price">
                <div class="container">
                    <div class="single__price__title">Цены</div>
                    <div class="price__table">
                        <?php $info = [
                            ['Перманентный макияж брови (волоски/напыление)',['20 000 ₽'],''],
                            ['Коррекция перманентного макияжа бровей (до 3 месяцев)',['10 000 ₽'],''],
                            ['Обновление (рефреш) перманентного макияжа бровей (от 3 месяцев до 1 года)',['12 000 ₽'],''],
                        ];?>
                        <?php foreach ($info as $item):?>
                            <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                            <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                                <div class="price__tr__inner">
                                    <div class="price__td price__td--title"><?= $item[0];?></div>
                                    <?php if (count($item[1])>0):?>
                                        <div class="price__td price__td--val">
                                            <?php for ($i=0; $i < count($item[1]); $i++):?>
                                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                            <?php endfor;?>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="js-reveal-slideUp btn__y__wrap">
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>
                </div>
            </div>



            <?php include 'parts/components/another__service.php'; ?>

        </section>


        <?php include 'parts/main/footer.php'; ?>

    </body>
</html>