jQuery(document).ready(function ($) {

    $(".js-reveal-slideUp") && ScrollReveal().reveal('.js-reveal-slideUp', {
        distance: "50%",
        origin: "bottom",
        opacity: 0,
        duration: 1000,
    });

    $(".js-reveal-slideUp--specialists").each(function(el){
        let delay = parseInt($(this).data('delay'));
        ScrollReveal().reveal($(this), {
            distance: "50%",
            origin: "bottom",
            opacity: 0,
            duration: 1000,
            delay: delay
        })
    });


    $(".js-reveal-slideUpDelay").each(function(el){
        let delay = parseInt($(this).data('delay'));
        ScrollReveal().reveal($(this), {
            distance: "50%",
            origin: "bottom",
            opacity: 0,
            duration: 1000,
            delay: delay
        });
    });


    $(".js-reveal-scale") && ScrollReveal().reveal(".js-reveal-scale", {
        scale: .5,
        opacity: 0,
        origin: "bottom",
        distance: "50%",
    }),



    $(".js-reveal-slideDown") && ScrollReveal().reveal('.js-reveal-slideDown', {
        distance: "50%",
        origin: "top",
        opacity: 0,
        duration: 1000,
    });

    $(".js-reveal-slideRight") && ScrollReveal().reveal('.js-reveal-slideRight', {
        distance: "50%",
        origin: "right",
        opacity: 0,
        duration: 1000,
    });
    $(".js-reveal-slideLeft") && ScrollReveal().reveal('.js-reveal-slideLeft', {
        distance: "50%",
        origin: "left",
        opacity: 0,
        duration: 1000,
    });
});
