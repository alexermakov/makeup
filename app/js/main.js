$(function () {
    $(window).on("load",function(){
        $('body').addClass('loaded')
    })

    $('.js__phone__mask').each(function(){
        $(this).inputmask("+7 999 999 9999");  //static mask
    })


    if ($('#js__map').length) {
        ymaps.ready(initMap);
    }

    function initMap() {
        var myMap = new ymaps.Map("js__map", {
            center: [55.730887, 37.459038],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        })


        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
                iconLayout: 'default#image',
                iconImageHref: 'images/icons/pointer.svg',
                iconImageSize: [22, 28],
                iconImageOffset: [-11, -14]
            }),

            myMap.geoObjects.add(myPlacemark)


    }




    $('.js_laser__t_0__slider__main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        swipeToSlide: true,
        asNavFor: $('.js_laser__t_0__slider__add'),
    })


    $('.js_laser__t_0__slider__add').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: $('.js_laser__t_0__slider__main'),
        dots: false,
        arrows: false,
        centerMode: false,
        focusOnSelect: true,
        swipeToSlide: true,
    })



    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        closeButton: 'inside',
    });

    $('.js_form').submit(function () {
        if ($(this)[0].checkValidity()) {
            const form = $(this);
            const formData = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "sendMail.php",
                data: formData,
                success: function () {
                    form[0].reset();
                    Fancybox.close();
                    Fancybox.show([{
                        src: "#js_modal__thanks"
                    }]);
                }
            });
        }
    });


    let slickResultParam1 = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        swipeToSlide: true,
        dots: true,
        infinite:false,
        autoplay: true,
        autoplaySpeed: 3000,
    };



    let slickResultParam2 = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        swipeToSlide: true,
        dots: true,
        infinite:false,
        autoplay: true,
        autoplaySpeed: 3000,

    };

    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });

    $('.js_single__results__btns  a').click(function (e) {
        e.preventDefault()
        if (!$(this).hasClass('active')) {
            $('.js_single__results__btns a').removeClass('active')
            $(this).addClass('active')
            let ind = $(this).index();

            $('.js_single__results__list .single__results__block').fadeOut(200, function () {
                $(this).removeClass('active')
            });

            $('.js_single__results__list .single__results__block').eq(ind).fadeIn(200, function () {

                let slider1 = $('.js_single__results__slider');
                let slider2 = $('.js_single__results__slider__video');

                slider1.slick('unslick')
                slider2.slick('unslick')



                slider1.slick(slickResultParam1)
                slider2.slick(slickResultParam2)

                $(this).addClass('active')

            })

        }
    })

    let slider1 = $('.js_single__results__slider');
    let slider2 = $('.js_single__results__slider__video');



    slider1.slick(slickResultParam1)
    slider2.slick(slickResultParam2)

    $('.js_single__history__list').slick({
        dots: false,
        arrows: true,
        slidesToShow: 3,
        swipeToSlide: true,
        prevArrow: $('.js_single__history__arrows .single__history__arrow:first-child'),
        nextArrow: $('.js_single__history__arrows .single__history__arrow:last-child'),
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 851,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })






    $('.js_btn__contact__interior a').click((function (e) {
        e.preventDefault()
        if (!$(this).hasClass('active')) {
            $('.js_btn__contact__interior a').removeClass('active')
            $(this).addClass('active')
            let ind = $(this).index();

            $('.js_contact__interior__list .contact__interior__item').fadeOut(200, function () {
                $(this).removeClass('active')
            });

            $('.js_contact__interior__list .contact__interior__item').eq(ind).fadeIn(200, function () {


                let slider = $('.js_contact__interior__list .contact__interior__item').eq(ind).find('.js_contact__interior__slider');
                let subSlider = $('.js_contact__interior__list .contact__interior__item').eq(ind).find('.js_contact__interior__sub__slider');

                slider.slick('unslick')
                subSlider.slick('unslick')

                slider.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    fade: true,
                    swipeToSlide: true,
                    asNavFor: subSlider,
                })


                subSlider.slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    asNavFor: slider,
                    dots: false,
                    arrows: false,
                    centerMode: false,
                    focusOnSelect: true,
                    swipeToSlide: true,
                    responsive: [{
                        breakpoint: 801,
                        settings: {
                            slidesToShow: 3,
                            dots: true
                        }
                    }]
                })

                $(this).addClass('active')

            })

        }
    }))



    $('.js_contact__interior__list .contact__interior__item').each(function (index, element) {
        let slider = $(this).find('.js_contact__interior__slider');
        let subSlider = $(this).find('.js_contact__interior__sub__slider');
        slider.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            swipeToSlide: true,
            asNavFor: subSlider,
        });


        subSlider.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: slider,
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            swipeToSlide: true,
            responsive: [{
                breakpoint: 801,
                settings: {
                    slidesToShow: 3,
                    dots: true
                }
            }]
        });

    });


    $('.js_single__tags a').click(function(e){
        e.preventDefault();
        let el = $($(this).attr('href'));
        let transform = el.css('transform').replace('matrix(','').replace(')','').split(',')
        offset = !!parseInt(el.css('opacity')) ? 20 : parseInt(transform[transform.length-1].trim()) + 20;

        if ($(window).width()<=800){
            offset += $('.header_main').outerHeight();
        }
        console.log(!!parseInt(el.css('opacity')))
        console.log(offset)
        $('html,body').animate({scrollTop: el.offset().top - offset},400);
    })





    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu__overlay').toggleClass('active')
    });


    function do_another__service__slider() {
        let $slider = $('.js_another__service__list');
        if ($(window).width() > 550) {
            if ($slider.hasClass('slick-initialized')) {
                $slider.slick('unslick')
            }
        } else {
            if (!$slider.hasClass('slick-initialized')) {
                $slider.slick({
                    rows: 3,
                    slidesToShow: 1,
                    dots: true,
                    arrows: true,
                })
            }
        }
    }

    $(window).resize(function () {
        do_another__service__slider()
    })

    do_another__service__slider()


});