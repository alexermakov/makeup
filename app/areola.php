<!DOCTYPE html>
<html lang="ru-RU">

    <head>
        <title>Арелола</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body class='page__service'>
        <?php include 'parts/main/header.php'; ?>



        <section class="main__section main__section--service">


            <div class="single__promo">
                <img class='single__promo__bg' src="images/pages/service/areola/1.jpg">
                <div class="container">
                    <?= breadcrumbs(['Главная','Ареола']);?>

                    <div class="single__promo__wrap">
                        <div class="single__promo__info js-reveal-slideLeft">
                            <h1 class="title_x single__promo__title">Красивые ареолы <br>Бережные процедуры</h1>
                            <div class="single__promo__text">
                                <p>Перманентный макияж ареолы это чаще всего эстетические цели. Все чаще к нам обращаются по рекомендации врачей, после или вместо операции.</p>
                            </div>
                            <a data-fancybox data-src="#js__modal__call"
                                class="btn__default btn__x btn__y--service js__modal">
                                <span class="btn__y__inner">Получить консультацию</span>
                            </a>
                        </div>

                        <div class="single__promo__image js-reveal-slideRight">
                            <div class="single__promo__image__inner">
                                <img src="images/pages/service/areola/2.jpg" class="single__promo__image__bg">
                                <div class="single__promo__image__info">
                                    <div class="single__promo__image__icon">
                                        <img src="images/pages/service/areola/3.svg">
                                    </div>
                                    <div class="single__promo__image__text">Арелола</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="single__tags js_single__tags js-reveal-slideUp">
                <div class="container">

                    <a href=".js_single_anco__text_0">Камуфляж ареол</a>
                    <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                    <a href=".js_single_ancor_histroy">Истории</a>
                    <a href=".js_single_anco__text_1">Какие бывают рубцы</a>
                    <a href=".js_single_anco__text_2">С какими задачами к нам обращаются</a>
                    <a href=".js_single_anco__text_3">Сколько шагов</a>
                    <a href=".js_single_anco__text_4">Противопоказания</a>
                    <a href=".js_single_anco__text_5">О процедуре</a>
                    <a href=".js_single_ancor_price">Цены</a>
                </div>
            </div>


            <div class="single__areols__text_1 js_single_anco__text_0">
                <div class="container">
                    <div class="single__areols__text_1__info js-reveal-slideLeft">
                        <div class="single__areols__text_1__title">Делать камуфляж ареол:</div>
                        <div class="single__areols__text_1__text">
                            <ul>
                                <li>Девушкам, которые не довольны оттенками ареолы.</li>
                                <li>Если в зоне Груди пигментация.</li>
                                <li>Неровности груди, ареолы разного размера.</li>
                                <li>Рубцы после различных видов пластики груди.</li>
                                <li>С целью эстетического улучшения груди.</li>
                                <li>После удаления груди.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="single__areols__text_1__image js-reveal-slideRight">
                        <img src="images/pages/service/areola/text/1.jpg" alt="">
                    </div>
                </div>
            </div>


            <div class="single__results js_single_ancor_result js-reveal-slideUp">
                <div class="container">
                    <div class="single__results__title">Результаты наших клиентов</div>
                    <div class="single__results__btns js_single__results__btns">
                        <a href="" class='active'>Галерея</a>
                        <a href="">Видео</a>
                    </div>
                    <div class="single__results__list js_single__results__list">
                        <div class="single__results__block active">
                            <div class="single__results__slider js_single__results__slider">
                                <?php for ($i=0; $i < 7; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <div class="single__results__item__col">
                                                <a href="images/pages/service/areola/result/<?= $i + 1;?>-0.jpg" data-fancybox='single__results--<?= $i;?>' class="single__results__item__col__image">
                                                    <img src="images/pages/service/areola/result/<?= $i + 1;?>-0.jpg">
                                                </a>
                                                <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                            </div>
                                            <div class="single__results__item__col">
                                                <a href="images/pages/service/areola/result/<?= $i + 1;?>-1.jpg" data-fancybox='single__results--<?= $i;?>' class="single__results__item__col__image">
                                                    <img src="images/pages/service/areola/result/<?= $i + 1;?>-1.jpg">
                                                </a>
                                                <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                        <div class="single__results__block">
                            <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                                <?php for ($i=0; $i < 7; $i++):?>
                                    <div class="single__results__slide">
                                        <div class="single__results__item">
                                            <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                                <img src="images/pages/service/lips/video/1.jpg">
                                            </a>
                                        </div>
                                    </div>
                                <?php endfor;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="single__history js_single_ancor_histroy js-reveal-slideUp">
                <div class="container">
                    <div class="single__history__top">
                        <div class="single__history__title">Истории</div>
                        <div class="single__history__arrows js_single__history__arrows">
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                            <button class='single__history__arrow'>
                                <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </button>
                        </div>
                    </div>

                    <div class="single__history__list js_single__history__list">

                        <?php for ($i=0; $i < 5; $i++):?>
                            <div class="single__history__item__wrap">
                                <div class="single__history__item">
                                    <div class="single__history__images">
                                        <div class="single__history__image">
                                            <img src="images/pages/service/areola/history/<?= $i % 3 + 1;?>-0.jpg">
                                            <div class="single__history__image__text">До</div>
                                        </div>
                                        <div class="single__history__image">
                                            <img src="images/pages/service/areola/history/<?= $i % 3 + 1;?>-1.jpg">
                                            <div class="single__history__image__text">После</div>
                                        </div>
                                    </div>
                                    <div class="single__history__info">
                                        <div class="single__history__info__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                                        <div class="single__history__info__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mauris cras ultrices ut sed nec nec, lacus, in. In tellus ut eget leo elit. Neque diam dictum quis et vehicula sit leo fermentum imperdiet.</div>
                                        <div class="single__history__info__who">
                                            <div class="single__history__info__who__image">
                                                <img src="images/__content/about.jpg">
                                            </div>
                                            <div class="single__history__info__who__title">
                                                <div class="single__history__info__who__value">Мастер:</div>
                                                <div class="single__history__info__who__name">Имя Фамилия</div>
                                            </div>
                                        </div>

                                        <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                                    </div>
                                </div>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>


            <div class="single__areols__tt">
                <div class="single__areols__t_1 js_single_anco__text_1">
                    <div class="container">
                        <div class="single__areols__t_1__info js-reveal-slideUpDelay" data-delay='100'>
                            <div class="single__areols__t_1__title">Какие бывают <br>рубцы на ареоле?</div>
                            <div class="single__areols__t_1__text">
                                <p>Мы работаем не со всеми видами рубцов на теле Так как к некоторым видам рубцов не применим камуфляж. Большинство рубцов подлежат камуфляжу, но если рубец келоидного типа, это означает что камуфляж невозможен. Часто мы назначаем терапию по улучшению структуры рубцов, и только после того как они станут мягкими, мы приступаем к камуфляжу.</p>

                                <p>Работать с рубцами рекомендовано после 10 — 12 месяцев после операции. У каждого рубца существует период формирования. И чем он более долгий, тем лучше. От года и более, это максимально безопасный период, когда перманентный макияж ареолы будет иметь наилучший результат.</p>

                                <p>Процедура не применима в течении года после кормления грудью, так как молочные железы и гормоны, после такого важного процесса, должны сначала полностью прийти в норму.</p>
                            </div>
                        </div>
                        <div class="single__areols__t_1__image js-reveal-slideUpDelay" data-delay='300'>
                            <div class="single__areols__t_1__image__inner">
                                <img src="images/pages/service/areola/text/2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single__areols__t_1 single__areols__t_1--reverse js_single_anco__text_2">
                    <div class="container">
                        <div class="single__areols__t_1__info js-reveal-slideUpDelay" data-delay='300'>
                            <div class="single__areols__t_1__title">С какими задачами к&nbsp;нам обращаются?</div>
                            <div class="single__areols__t_1__text">
                               <p>Чаще всего камуфляж ареол интересует девушек, которые хотят что-либо скрыть или эстетически улучшить свои ареолы.</p>

                               <p>Для каждого участка ареолы существуют свои оттенки. Под ваш природный цвет кожи, мы подберем самые выигрышные пигменты. Оттенки зависят от задачи: сделать тон кожи более ровным, восстановить полностью ареолу или скрыть витилиго.</p>

                               <p>Важными гостями для нас являются девушки, которые перенесли удаление груди. Процедуры после таких операций мы проводим все чаще после направления от врачей, которые знают, что наши методы и подход гарантируют безопасность. Именно врач проводивший операцию принимает решение о том, что ткани готовы к эстетическому камуфляжу.</p>
                            </div>
                        </div>
                        <div class="single__areols__t_1__image js-reveal-slideUpDelay" data-delay='100'>
                            <div class="single__areols__t_1__image__inner">
                                <img src="images/pages/service/areola/text/3.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>




                <div class="single__areols__t_2 js_single_anco__text_3">
                    <div class="container">
                        <div class="single__areols__t_2__info js-reveal-slideUpDelay" data-delay='100'>
                            <div class="single__areols__t_2__title">Сколько шагов</div>
                            <div class="single__areols__t_2__text">
                               <ol>
                                    <li>На консультации мы подберем хороший оттенок из 50 оттенков.</li>
                                    <li>Определим количество ваших визитов и над чем нам предстоит работать в каждом из них.</li>
                                    <li>Эскиз.</li>
                                    <li>Поставим дату и обсудим какой требуется подготовительный уход.</li>
                                    <li>Предоставим вам процедуру и рекомендации по уходу.</li>
                               </ol>
                            </div>
                        </div>
                        <div class="single__areols__t_2__image js-reveal-slideUpDelay" data-delay='300'>
                            <div class="single__areols__t_2__image__inner">
                                <img src="images/pages/service/areola/text/4.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="single__areols__t_3 js_single_anco__text_4">
                    <div class="container">
                        <div class="single__areols__t_3__image js-reveal-slideUpDelay" data-delay='100'>
                            <img src="images/pages/service/areola/text/5.jpg?1" alt="">
                        </div>
                        <div class="single__areols__t_3__info js-reveal-slideUpDelay" data-delay='300'>
                            <div class="single__areols__t_3__title">У эстетического камуфляжа ареолы есть противопоказания: </div>
                            <div class="single__areols__t_3__text">
                               <ul>
                                    <li>Беременность, кормление</li>
                                    <li>Заболевания крови</li>
                                    <li>Сахарный Диабет </li>
                                    <li>Склонность кожи к рубцеванию</li>
                                    <li>Аллергические реакции</li>
                                    <li>Онкология </li>
                               </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single__areols__t_4 js_single_anco__text_5">
                    <div class="container">

                        <div class="single__areols__t_4__info js-reveal-slideUpDelay" data-delay='300'>
                            <div class="single__areols__t_4__title">Больше о  процедуре</div>
                            <div class="single__areols__t_4__text">
                                <p>Перед процедурой вы заполните документы, чтобы мы могли узнать о вашем здоровье.</p>

                                <p>Мы пообщаемся о вашем здоровье и образе жизни, так как они влияют на долговечность рисунка.</p>

                                <p>После официальных бумаг вас ждет консультация с подбором формы и цвета, используя оборудование для того, чтобы разметить и нарисовать ваши ареолы.</p>

                                <p>Наноситься крем обезболивающий зону работы.</p>

                                <p>Результат — пигментированная ареола и камуфляж. Благодаря этому ваши ареолы не будут иметь четкого контура. В основном вы будете чувствовать заботливые руки мастера и вибрацию. Обычно процедура занимает час, в зависимости от объёма работы. Коррекция требуется через полтора месяца.</p>
                            </div>
                        </div>
                        <div class="single__areols__t_4__image js-reveal-slideUpDelay" data-delay='100'>
                            <img src="images/pages/service/areola/text/6.jpg" alt="">
                        </div>
                    </div>
                </div>


            </div>





            <div class="single__price js_single_ancor_price">
                <div class="container">
                    <div class="single__price__title">Цены</div>
                    <div class="price__table">
                        <?php $info = [
                            ['Камуфляж ареол',['25 000 ₽'],''],
                            ['Коррекция ареол',['15 000 ₽'],''],
                            ['Третья процедура камуфляжа ареол( в течении 6 месяцев от первой процедуры)',['10 000 ₽'],''],
                        ];?>
                        <?php foreach ($info as $item):?>
                            <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                            <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                                <div class="price__tr__inner">
                                    <div class="price__td price__td--title"><?= $item[0];?></div>
                                    <?php if (count($item[1])>0):?>
                                        <div class="price__td price__td--val">
                                            <?php for ($i=0; $i < count($item[1]); $i++):?>
                                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                            <?php endfor;?>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>

                    <div class="js-reveal-slideUp btn__y__wrap">
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>
                </div>
            </div>


            <?php include 'parts/components/another__service.php'; ?>

        </section>


        <?php include 'parts/main/footer.php'; ?>

    </body>
</html>