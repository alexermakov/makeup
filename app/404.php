<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>404</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body>
        <?php include 'parts/main/header.php'; ?>



        <section class="main__section main__section--404">
            <div class="container">
                <?= breadcrumbs(['Главная','404']);?>
                <div class="page__404__content">
                    <div class="page__404__info js-reveal-slideLeft">
                        <div class="title_x page__404__info__title">404</div>
                        <div class="page__404__info__text">Страницы, которую вы искали, нет на нашем сайте. <br>Возможно вы ввели неправильный адрес или страница была удалена.</div>
                        <a href="index.php" class="btn_default btn__x btn__404">Вернуться на главную</a>
                    </div>

                    <div class="page__404__image js-reveal-slideRight">
                        <div class="page__404__image__inner">
                            <div class="page__404__image__inner__view"></div>
                            <div class="single__promo__image__info">
                                <div class="single__promo__image__icon">
                                    <img src="images/pages/404/1.svg">
                                </div>
                                <div class="single__promo__image__text">СТРАНИЦА НЕ НАЙДЕНА</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <?php include 'parts/main/footer.php'; ?>

    </body>
</html>