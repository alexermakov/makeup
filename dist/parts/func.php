<?php

function breadcrumbs($links){
    $text = '<div class="breadcrumbs"><ul>';

    for ($i=0; $i < count($links)-1; $i++) {
        $text .='<li><a title='.$links[$i].' href="javascript:void(0)">'.$links[$i].'</a></li>';
    }
    $text .='<li><span>'.$links[count($links)-1].'</span></li>';

    $text .='</ul></div>';
    return $text;
}
?>