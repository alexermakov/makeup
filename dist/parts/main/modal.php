<div class="js__modal__menu modal__menu">
    <div class="modal__menu__inner">

        <div class="modal__menu__top">
            <div class="modal__menu__top__text">Меню</div>
            <button class="modal__menu__btn_close btn__close js_modal__menu__btn_close"></button>
        </div>



        <div class="modal__menu__nav">
            <ul>
                <li><a href="brows.php">Брови</a></li>
                <li><a href="lips.php">Губы</a></li>
                <li><a href="vitiligo.php">Витилиго </a></li>
                <li><a href="scars.php">Шрамы/ рубцы </a></li>
                <li><a href="areola.php">Ареолы </a></li>
                <li><a href="laser.php">Лазерная эпиляция </a></li>
                <li><a href="removal.php">Удаление перманета </a></li>
                <li><a href="lamination.php">Ламинирование</a></li>
            </ul>
            <ul>
                <li><a href="contact.php" title='Контакты'>Контакты</a></li>
                <li><a href="price.php" title='Цены'>Цены</a></li>
                <li><a href="specialists.php" title='Специалист'>Специалист</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="modal__menu__overlay js_modal__menu__overlay"></div>



<div id="js__modal__more" class="modal__default modal__text">
    <div class="modal__inner">
        <div class="modal__block_before_after">
            <div class="modal__block_before_after__item">
                <div class="modal__block_before_after__item__inner">
                    <img src="images/pages/service/brows/result/1-0.jpg" alt="">
                </div>
                <div class="modal__block_before_after__item__label">До</div>
            </div>
            <div class="modal__block_before_after__item">
                <div class="modal__block_before_after__item__inner">
                    <img src="images/pages/service/brows/result/1-1.jpg" alt="">
                </div>
                <div class="modal__block_before_after__item__label">После</div>
            </div>
        </div>
        <div class="modal__block_text__title">Обидно же, когда вас все время спрашивают, почему вы такие грустные?</div>
        <div class="modal__block_text">
            <p>Вот и эту девушка расстраивали такие вопросы. На самом деле она очень даже веселая, но из-за того, что у нее были тонкие опущенные брови, казалось, что у нее очень печальный взгляд. Как решить эту проблему? Изменить свой взгляд, причем именно в визуальном плане, а не только на жизнь (хотя некоторым это бывает тоже актуально).</p>

            <p>Клиентка попросила меня сделать ей перманентный макияж бровей так, чтобы поднять и уголки глаз, и уголки губ, и вообще чтобы все лицо стало улыбающимся. Мы вместе рисовали эскиз, создавали форму и смотрели анатомически, насколько высоко можно построить рисунок.</p>

            <p>Дело в том, что когда у человека очень низкие глазницы и невыпуклый лоб, мышцы на бровях крепятся достаточно низко, и, соответственно, внешний уголок хвостика брови всегда спадающий. А вместе в опущенными уголками глаз и губ это дает ощущение, что нижняя треть лица шире, чем есть на самом деле, и все линии лица идут книзу.</p>

            <p>Мы отрисовали эскиз и попробовали приподнять все это дело максимально высоко. Самое важное в такой работе, когда надо приподнять нижнюю треть лица, — это построить внешний уголок брови так, чтобы он был выше головки брови (часть ближайшая к переносице), чтобы он как будто взлетал. Тогда это как бы поднимает все лицо кверху, приподнимает и взгляд, и скулу, и подбородок.</p>

            <p>Получился очень классный образ, взгляд стал высоким, глаза стали смеющимися и грустный образ исчез. Девушка осталась очень довольна. На фотографии ПОСЛЕ сразу видно, как у нее поднялся взгляд и насколько уже стало смотреться лицо из-за того, что брови стали выше и шире.</p>

            <p>В работе я использовала метод «волоски», чтобы визуально брови не отличались от природных. Родные волоски также присутствуют по всей форме, мы их сочетаем с рисунком и в итоге получаем красивые пушистые брови, которые неотличимы от натуральных.</p>

            <p>По оттенку мы работали русо-коричневым цветом, чтобы и подчеркнуть цвет глаз, и попасть в колористику образа — темные корни и светлые кончики волос. Когда такая работа проводится на светлой коже, очень важно использовать пигменты, которые не имеют серого подтона, а имеют теплый коричневый поддон на основе желтого, чтобы со временем брови не переходили в прохладный асфальтовый цвет, а оставались по оттенку мягкими, теплыми и пушистыми. Это дает рисунку объем.</p>

            <p>Работа такая всегда проводится в 2 этапа, требуется обязательно коррекция, потому что волоски — очень тонкая работа по исполнению, и обязательно через некоторое время нужно посмотреть, насколько качественно они зажили. Для того, чтобы они долго и хорошо носились, необходима коррекция через 2 месяца, а потом, для поддержания качества — через 2 года.</p>
        </div>

        <div class="modal__block__bottom">
            <div class="modal__block__who__wrap">
                <div class="modal__block__who">
                    <a href='specialist.php' class="modal__block__who__image">
                        <img src="images/__content/about.jpg">
                    </a>
                    <div class="modal__block__who__title">
                        <div class="modal__block__who__title__value">Мастер:</div>
                        <div class="modal__block__who__title__name">Имя Фамилия</div>
                    </div>
                </div>

                <div class="modal__block__more__image">
                    <div class="modal__block__more__image__inner">
                        <img src="http://localhost/makeup/dist/images/pages/service/laser/text/8.jpg" alt="">
                    </div>
                </div>


                <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x modal__text__btn js__modal">
                    <span class="btn__y__inner">Получить консультацию</span>
                </a>
            </div>

        </div>

    </div>
</div>





<div id="js__modal__route" class="modal__default modal__route">
    <div class="modal__inner">
        <div class="modal__title">Как добраться</div>
        <div class="modal__text__route">
            <div class="modal__text__route__col">
                <div class="modal__text__route__col__title">На автомобиле</div>
                <div class="modal__text__route__col__list">
                    <ul>
                        <li>Двигаясь по Кутузовскому проспекту вам нужно будет доехать до староможаййского шоссе и завернуть на него. </li>
                        <li>Сделать петлю через минскую улицу и повернуть на лево на улицу Кастанаевская.</li>
                        <li>Ехать прямо не доезжая дома Кастанаевская 53, поворачиваете на лево и доезжаете до шлагбаума дома улицы Кастанаевская 55к1.</li>
                        <li>Чтобы заехать во двор, позвоните по номеру +7 (926) 762 61 07, вам откроют шлагбаум.</li>
                        <li>Наша студия находится со стороны дороги , вывеска : Беz Макияжа</li>
                    </ul>
                </div>
            </div>

            <div class="modal__text__route__col">
                <div class="modal__text__route__col__title">На метро</div>
                <div class="modal__text__route__col__list">
                    <ul>
                        <li>Двигаясь из центра по Арбатско-покровской линии (синяя ветка) , вам нужно доехать до станции Славянский бульвар.</li>
                        <li>Выход #2 , через мцд1 славянский бульвар (из стеклянных дверей на лево) выход на улицу Герасима Курина.</li>
                        <li>Далее по улице Герасима Курина идите прямо по аллее до бульвара Солдатенкова.</li>
                        <li>По пешеходному переходу проходите прямо до дома кастанаевская 55к1.</li>
                        <li>Наша студия находится со стороны дороги бульвара Солдатенкова в доме ул. Кастанаевская 55к1 вывеска : БеzМакияжа</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="js__modal__call" class="modal__default modal__call">
    <div class="modal__inner">
        <div class="modal__title__call">Оставьте заявку</div>
        <div class="modal__text__call">Мы работаем с <b>9:00</b> до <b>20:00</b> <br>и перезвоним Вам в ближайшее время</div>
        <form action="javascript:void(0)" class="modal__form js_form">

            <div class="form__field">
                <input type="text" name="name" required placeholder="Имя">
            </div>

            <div class="form__field">
                <input type="tel" name="phone" required class='js__phone__mask' placeholder="+7 ___ ___ ____">
            </div>

            <div class="form__field form__field--btn">
                <button class="btn__default btn__x btn__modal__call">Получить консультацию</button>
            </div>

        </form>
    </div>
</div>



<!-- окно Спасибо Покупка в один клик -->

<div id="js_modal__thanks" class="modal__default modal__thanks">
    <div class="modal__inner">
        <div class="modal__title__call">Спасибо!</div>
        <div class="modal__text__call">Ваша заявка принята!</div>

        <svg class='modal__icon__call' width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle opacity="0.3" cx="50" cy="50" r="50" fill="#FB678D"/>
            <circle cx="50" cy="50" r="42" fill="#FB678D"/>
            <path d="M52.6977 45.6827C59.7551 38.5829 60.9987 37.3873 61.5089 37.2105C62.3994 36.9019 63.3105 36.9335 64.0904 37.2997C65.7569 38.0823 66.4657 40.0491 65.6792 41.7094C65.231 42.6553 46.6912 61.3325 45.8003 61.7355C45.0192 62.0888 43.9388 62.0881 43.1602 61.7339C42.7221 61.5346 41.3932 60.2863 38.0967 56.9778C33.1565 52.0194 32.9971 51.8125 33 50.3621C33.002 49.3349 33.3178 48.6374 34.0911 47.9509C34.7744 47.3444 35.4357 47.0993 36.3773 47.1036C37.5653 47.1089 37.9805 47.4178 41.3453 50.7987L44.4805 53.9491L52.6977 45.6827Z" fill="white"/>
        </svg>



        <button class="btn__default btn__x__border btn__form_close js__btn_close__modal">Хорошо!</button>

    </div>
</div>