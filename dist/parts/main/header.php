<header class="header_main">
    <div class="container">

        <a href="index.php" class="header__logo">
            <img src="images/logo.svg" alt="main logo">
        </a>

        <div class="header__menu__top">
            <ul>
                <li><a href="specialists.php">Мастера</a></li>
                <li><a href="price.php">Цены</a></li>
                <li><a href="contact.php">Контакты</a></li>
            </ul>
        </div>

        <div class="header__social">
            <a href="" target="_blank">
                <img src="images/icons/social/you.svg">
            </a>
            <a href="" target="_blank">
                <img src="images/icons/social/vk.svg">
            </a>
        </div>

        <div class="header__phone">
            <a href="tel:+7 (926) 762 61 07">+7 (926) 762 61 07</a>
        </div>

        <button class="header__btn_menu btn_menu js_btn_menu">
            <span></span>
            <span></span>
            <span></span>
        </button>
    </div>



</header>

<nav class="main__menu">
    <div class="container">
        <div class="header__menu__category">
            <ul>
                <li><a href="scars.php">Шрамы/ рубцы </a></li>
                <li><a href="areola.php">Ареолы </a></li>
                <li><a href="lamination.php">Ламинирование</a></li>
                <li><a href="laser.php">Лазерная эпиляция </a></li>
                <li><a href="brows.php">Брови</a></li>
                <li><a href="lips.php">Губы</a></li>
                <li><a href="vitiligo.php">Витилиго </a></li>
                <li><a href="removal.php">Удаление </a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- end header -->
