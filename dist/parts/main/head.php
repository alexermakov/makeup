<meta charset="UTF-8">
<meta name="robots" content="index, follow" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<link rel="icon" type="image/png" sizes="128x128" href="images/favicon/favicon-128x128.png">
<link rel="icon" type="image/png" sizes="64x64" href="images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#224fc2">
<meta name="theme-color" content="#ffffff">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->
<link rel="stylesheet" href="css/main.min.css?<?= time();?>">

<?php include 'parts/func.php'; ?>