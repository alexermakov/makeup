<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Специалисты</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body>
    <?php include 'parts/main/header.php'; ?>

    <section class="main__section main__section--specialists">
        <div class="container">
            <?= breadcrumbs(['Главная','Специалист']);?>
            <h1 class="title_x --hide">Мастера</h1>
            <div class="specialist__list">
                <?php for ($i=0; $i < 6; $i++):?>
                <a href='specialist.php' class="specialist__card js-reveal-slideUp--specialists" data-delay=<?= $i % 3 *100;?>>
                    <div class="specialist__card__inner">
                        <div class="specialist__card__top__row">
                            <div class="specialist__card__image">
                                <img src="images/__content/about.jpg">
                            </div>
                            <div class="specialist__card__name">Кусакина Татьяна Дмитриевна</div>
                        </div>
                        <div class="specialist__card__info">
                            <div class="specialist__card__info__item">
                                <div class="specialist__card__info__item__title">Специальность:</div>
                                <div class="specialist__card__info__item__text">Главный мастер Студии на Кутузовском проспекте</div>
                            </div>
                            <div class="specialist__card__info__item">
                                <div class="specialist__card__info__item__title">Опыт работы:</div>
                                <div class="specialist__card__info__item__text">15 лет</div>
                            </div>
                            <div class="specialist__card__info__item">
                                <div class="specialist__card__info__item__title">Образование:</div>
                                <div class="specialist__card__info__item__text">Кафедра управления Медицинским Учреждением НИИ Москвы ГУПХГБУ</div>
                            </div>
                        </div>

                        <div class="btn__default specialist__card__more">Читать о специалисте</div>
                    </div>
                </a>
                <?php endfor;?>
            </div>
        </div>
    </section>


    <?php include 'parts/main/footer.php'; ?>

</body>

</html>