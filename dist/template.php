<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Цены</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body>
    <?php include 'parts/main/header.php'; ?>



    <section class="main__section main__section--price">
        <div class="container">
            <?= breadcrumbs(['Главная','Цены']);?>
            <h1 class="title_x">Цены</h1>

        </div>
    </section>


    <?php include 'parts/main/footer.php'; ?>

</body>

</html>