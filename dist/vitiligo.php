<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Витилиго</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body class='page__service'>
    <?php include 'parts/main/header.php'; ?>



    <section class="main__section main__section--service">


        <div class="single__promo">
            <img class='single__promo__bg' src="images/pages/service/vitiligo/1.jpg">
            <div class="container">
                <?= breadcrumbs(['Главная','Витилиго']);?>

                <div class="single__promo__wrap">
                    <div class="single__promo__info js-reveal-slideLeft">
                        <h1 class="title_x single__promo__title">Камуфляж витилиго</h1>
                        <div class="single__promo__text">
                            <ul>
                                <li>Подберем идентичные оттенки для разных участков вашего тела.</li>
                                <li>Используем легко переносимый анестезирующий крем, процедура пройдет без боли.
                                </li>
                                <li>Натуральные пигменты, которые сами испарятся из кожи в течении 3х лет.</li>
                            </ul>
                        </div>
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x btn__y--service js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>

                    <div class="single__promo__image js-reveal-slideRight">
                        <div class="single__promo__image__inner">
                            <img src="images/pages/service/vitiligo/2.jpg" class="single__promo__image__bg">
                            <div class="single__promo__image__info">
                                <div class="single__promo__image__icon">
                                    <img src="images/pages/service/vitiligo/3.svg">
                                </div>
                                <div class="single__promo__image__text">Витилиго</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="single__tags js_single__tags js-reveal-slideUp">
            <div class="container">
                <a href=".js_single_ancor_what_is">Что такое витилиго?</a>
                <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                <a href=".js_single_ancor_histroy">Истории</a>
                <a href=".js_single_ancor_price">Цены</a>
            </div>
        </div>




        <div class="single__text__2 js_single_ancor_what_is">
            <div class="container">
                <div class="single__text__2__info js-reveal-slideUpDelay" data-delay='100'>
                    <div class="single__text__2__text">
                        <p><b>Витилиго</b> — это заболевание, когда один или более участок кожи теряет свой пигмент. Лечение пятна витилиго — разделяется на терапевтический и эстетический уход. Потеря пигмента в коже зависит напрямую от уровня стресса и иммунной системы.</p>
                        <p>Камуфляж витилиго — эстетическая процедура, когда перманентный макияж наносят на участки потерявшие пигмент. До процедуры нужна консультация.</p>
                    </div>
                </div>
                <div class="single__text__2__image js-reveal-slideUpDelay" data-delay='300'>
                    <div class="single__text__2__image__inner">
                        <img src="images/pages/service/vitiligo/text/1.jpg">
                    </div>
                </div>
            </div>
        </div>



        <div class="single__results js_single_ancor_result js-reveal-slideUp">
            <div class="container">
                <div class="single__results__title">Результаты наших клиентов</div>
                <div class="single__results__btns js_single__results__btns">
                    <a href="" class='active'>Галерея</a>
                    <a href="">Видео</a>
                </div>
                <div class="single__results__list js_single__results__list">
                    <div class="single__results__block active">
                        <div class="single__results__slider js_single__results__slider">
                            <?php for ($i=0; $i < 7; $i++):?>
                            <div class="single__results__slide">
                                <div class="single__results__item">
                                    <div class="single__results__item__col">
                                        <div class="single__results__item__col__image">
                                            <img src="images/pages/service/vitiligo/result/<?= $i + 1;?>-0.jpg">
                                        </div>
                                        <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                    </div>
                                    <div class="single__results__item__col">
                                        <div class="single__results__item__col__image">
                                            <img src="images/pages/service/vitiligo/result/<?= $i + 1;?>-1.jpg">
                                        </div>
                                        <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                    </div>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>
                    </div>
                    <div class="single__results__block">
                        <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                            <?php for ($i=0; $i < 7; $i++):?>
                            <div class="single__results__slide">
                                <div class="single__results__item">
                                    <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                        <img src="images/pages/service/lips/video/1.jpg">
                                    </a>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="single__history js_single_ancor_histroy js-reveal-slideUp">
            <div class="container">
                <div class="single__history__top">
                    <div class="single__history__title">Истории</div>
                    <div class="single__history__arrows js_single__history__arrows">
                        <button class='single__history__arrow'>
                            <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                        <button class='single__history__arrow'>
                            <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                    </div>
                </div>

                <div class="single__history__list js_single__history__list">

                    <?php $info = [
                            ['Обидно же, когда вас все время спрашивают, почему вы такие грустные?','Вот и эту девушка расстраивали такие вопросы. На самом деле она очень даже веселая, но из-за того, что у нее были тонкие опущенные брови...'],
                            ['Вижу  возрастные изменения, помогите их убрать !','К нам обратилась дама 40+, и у нее был такой запрос: «Я вижу возрастные изменения и не знаю что с ними делать, так как не хочу прибегать к инъекциям. Скажите, пожалуйста, за счет каких...'],
                            ['Муж был против, но я решилась !','Иногда очень хочется что-то поменять в лице, но боишься реакции близких. Вот и эта девушка обратилась к нам с просьбой сделать такой перманентный макияж, чтобы его не заметил ее муж...'],
                        ];?>
                    <?php for ($i=0; $i < 5; $i++):?>
                    <div class="single__history__item__wrap">
                        <div class="single__history__item">
                            <div class="single__history__images">
                                <div class="single__history__image">
                                    <img src="images/pages/service/vitiligo/history/<?=$i % 3 + 1;?>-0.jpg">
                                    <div class="single__history__image__text">До</div>
                                </div>
                                <div class="single__history__image">
                                    <img src="images/pages/service/vitiligo/history/<?= $i % 3 + 1;?>-1.jpg">
                                    <div class="single__history__image__text">После</div>
                                </div>
                            </div>
                            <div class="single__history__info">
                                <div class="single__history__info__title"><?= $info[$i % 3][0];?></div>
                                <div class="single__history__info__text"><?= $info[$i % 3][1];?>.</div>
                                <div class="single__history__info__who">
                                    <div class="single__history__info__who__image">
                                        <img src="images/__content/about.jpg">
                                    </div>
                                    <div class="single__history__info__who__title">
                                        <div class="single__history__info__who__value">Мастер:</div>
                                        <div class="single__history__info__who__name">Кусакина Татьяна Дмитриевна</div>
                                    </div>
                                </div>

                                <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                            </div>
                        </div>
                    </div>
                    <?php endfor;?>
                </div>
            </div>
        </div>


        <div class="single__price js_single_ancor_price">
            <div class="container">
                <div class="single__price__title">Цены</div>
                <div class="price__table">
                    <?php $info = [
                            ['Камуфляж витилиго',['от 15 000 ₽'],''],
                            ['Расходники',['5 000 ₽'],''],
                        ];?>
                    <?php foreach ($info as $item):?>
                    <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                    <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                        <div class="price__tr__inner">
                            <div class="price__td price__td--title"><?= $item[0];?></div>
                            <?php if (count($item[1])>0):?>
                            <div class="price__td price__td--val">
                                <?php for ($i=0; $i < count($item[1]); $i++):?>
                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                <?php endfor;?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="js-reveal-slideUp btn__y__wrap">
                    <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                        <span class="btn__y__inner">Получить консультацию</span>
                    </a>
                </div>
            </div>
        </div>



        <?php include 'parts/components/another__service.php'; ?>

    </section>


    <?php include 'parts/main/footer.php'; ?>

</body>

</html>