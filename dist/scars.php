<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Лазерная эпиляция</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body class='page__service'>
    <?php include 'parts/main/header.php'; ?>



    <section class="main__section main__section--service">


        <div class="single__promo">
            <img class='single__promo__bg' src="images/pages/service/scars/1.jpg">
            <div class="container">
                <?= breadcrumbs(['Главная','Рубцы / Шрамы']);?>

                <div class="single__promo__wrap">
                    <div class="single__promo__info js-reveal-slideLeft">
                        <h1 class="title_x single__promo__title">Скроем шрамы / рубцы на&nbsp;75&nbsp;-&nbsp;95%</h1>
                        <div class="single__promo__text">
                            <ul>
                                <li>Камуфляж после того как прошла операция</li>
                                <li>После любых операций на теле / кесарева</li>
                                <li>Шрамы после травм</li>
                                <li>Следы от удаления родинок / ожогов</li>
                                <li>Рубцы в зоне лица / шеи / живота</li>
                                <li>Шрамы от порезов на руках и теле</li>
                                <li>Шрамы после удаления татуажа / татуировки</li>
                                <li>Шрамы пересадка: кожа / волосы</li>
                                <li>Шлифовка рубца лазером</li>
                                <li>Шрам губы</li>
                            </ul>
                        </div>
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x btn__y--service js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>

                    <div class="single__promo__image js-reveal-slideRight">
                        <div class="single__promo__image__inner">
                            <img src="images/pages/service/scars/2.jpg" class="single__promo__image__bg">
                            <div class="single__promo__image__info">
                                <div class="single__promo__image__icon">
                                    <img src="images/pages/service/scars/3.svg">
                                </div>
                                <div class="single__promo__image__text">Рубцы / Шрамы</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="single__tags js_single__tags js-reveal-slideUp">
            <div class="container">
                <a href=".js_single_ancor_what_is">Подробнее о процедуре</a>
                <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                <a href=".js_single_ancor_histroy">Истории</a>
                <a href=".js_single__text__scar_0">Долговечность результатов</a>
                <a href=".js_single__text__scar_1">Виды шрамов, за которые мы беремся</a>
                <a href=".js_single__text__scar_2">Виды шрамов, для которых комуфляж противопоказан</a>
                <a href=".js_single_ancor_price">Цены</a>
            </div>
        </div>




        <div class="single__text__scars js_single_ancor_what_is">
            <div class="container">
                <div class="single__text__scars__info js-reveal-slideLeft">
                    <div class="single__text__scars__title">Подробнее о процедуре</div>
                    <div class="single__text__scars__text">
                        <p>Эстетическая процедура, ее цель скрыть шрам / рубец.</p>
                        <p>В случае с эстетическими процедурами с самыми разными видами шрамов / рубцов, требуется 2-3 посещения.</p>
                        <p>Рубец это плотная по структуре ткань, слои нужно класть нескольким оттенками и различными методами, тогда кожа позволяет сохранить оттенок.</p>
                    </div>
                </div>
                <div class="single__text__scars__image js-reveal-slideRight">
                    <div class="single__text__scars__image__inner">
                        <img src="images/pages/service/scars/text/1.jpg">
                    </div>
                </div>
            </div>
        </div>




        <div class="single__results js_single_ancor_result js-reveal-slideUp">
            <div class="container">
                <div class="single__results__title">Результаты наших клиентов</div>
                <div class="single__results__btns js_single__results__btns">
                    <a href="" class='active'>Галерея</a>
                    <a href="">Видео</a>
                </div>
                <div class="single__results__list js_single__results__list">
                    <div class="single__results__block active">
                        <div class="single__results__slider js_single__results__slider">
                            <?php for ($i=0; $i < 7; $i++):?>
                            <div class="single__results__slide">
                                <div class="single__results__item">
                                    <div class="single__results__item__col">
                                        <div class="single__results__item__col__image">
                                            <img src="images/pages/service/scars/result/<?= $i + 1;?>-0.jpg">
                                        </div>
                                        <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                    </div>
                                    <div class="single__results__item__col">
                                        <div class="single__results__item__col__image">
                                            <img src="images/pages/service/scars/result/<?= $i + 1;?>-1.jpg">
                                        </div>
                                        <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                    </div>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>
                    </div>
                    <div class="single__results__block">
                        <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                            <?php for ($i=0; $i < 7; $i++):?>
                            <div class="single__results__slide">
                                <div class="single__results__item">
                                    <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                        <img src="images/pages/service/lips/video/1.jpg">
                                    </a>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="single__history js_single_ancor_histroy js-reveal-slideUp">
            <div class="container">
                <div class="single__history__top">
                    <div class="single__history__title">Истории</div>
                    <div class="single__history__arrows js_single__history__arrows">
                        <button class='single__history__arrow'>
                            <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                        <button class='single__history__arrow'>
                            <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                    </div>
                </div>

                <div class="single__history__list js_single__history__list">

                    <?php for ($i=0; $i < 5; $i++):?>
                    <div class="single__history__item__wrap">
                        <div class="single__history__item">
                            <div class="single__history__images">
                                <div class="single__history__image">
                                    <img src="images/pages/service/scars/history/<?= $i % 3 + 1;?>-0.jpg">
                                    <div class="single__history__image__text">До</div>
                                </div>
                                <div class="single__history__image">
                                    <img src="images/pages/service/scars/history/<?= $i % 3 + 1;?>-1.jpg">
                                    <div class="single__history__image__text">После</div>
                                </div>
                            </div>
                            <div class="single__history__info">
                                <div class="single__history__info__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                                <div class="single__history__info__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mauris cras ultrices ut sed nec nec, lacus, in. In tellus ut eget leo elit. Neque diam dictum quis et vehicula sit leo fermentum imperdiet.</div>
                                <div class="single__history__info__who">
                                    <div class="single__history__info__who__image">
                                        <img src="images/__content/about.jpg">
                                    </div>
                                    <div class="single__history__info__who__title">
                                        <div class="single__history__info__who__value">Мастер:</div>
                                        <div class="single__history__info__who__name">Имя Фамилия</div>
                                    </div>
                                </div>

                                <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                            </div>
                        </div>
                    </div>
                    <?php endfor;?>
                </div>
            </div>
        </div>


        <div class="single__text__scars_0">
            <div class="single__text__scars_0__row js_single__text__scar_0">
                <div class="container">
                    <div class="single__text__scars_0__info js-reveal-slideUpDelay" data-delay='100'>
                        <div class="single__text__scars_0__title">Долговечность результатов</div>
                        <div class="single__text__scars_0_text">
                            <p>После процедуры и коррекции, камуфляж держится два года в зависимости от типа кожи. Благодаря процедуре, цвет шрама / рубца сохранится другим навсегда.</p>
                            <p>Используем самые минеральные и гибридные пигменты, которые прекрасно ложатся, совпадают с тоном кожи и со временем уходят без изменения цвета, просто светлеют.</p>
                        </div>
                    </div>
                    <div class="single__text__scars_0__image js-reveal-slideUpDelay" data-delay='300'>
                        <img src="images/pages/service/scars/text/2.jpg">
                    </div>
                </div>
            </div>
            <div class="single__text__scars_0__row single__text__scars_0__row--reserse js_single__text__scar_1">
                <div class="container">
                    <div class="single__text__scars_0__info single__text__scars_0__info--2 js-reveal-slideUpDelay" data-delay='100'>
                        <div class="single__text__scars_0__title">Виды шрамов</div>
                        <div class="single__text__scars_0_text">
                            <p>Нормотрофический — считается самым подходящим для камуфляжа видом шрама. По уровню он на одном уровне с кожей.</p>

                            <p>Гипертрофический — вид рубца, который выступает над поверхностью и имеет светлый оттенок с пигментацией вокруг, часто бывает после операций, травм и ран.</p>

                            <p>Атрофический рубец — такой вид травмы, когда рубец сохранится ниже уровня кожи. Такой шрам имеет вид участка, измененный рубцом. Он сохранится в глубину по сравнению с соседними тканями. Растяжки это атрофические рубцы.</p>
                        </div>
                    </div>
                    <div class="single__text__scars_0__image single__text__scars_0__image--2 single__text__scars_0__image--align__bottom js-reveal-slideUpDelay" data-delay='300'>
                        <img src="images/pages/service/scars/text/3.png">
                    </div>
                </div>
            </div>

            <div class="single__text__scars_0__line js_single__text__scar_2 js-reveal-slideUp">
                <div class="container">
                    <div class="single__text__scars_0__line__title">
                        <svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="37.5" cy="37.5" r="37.5" fill="#FB678D" />
                            <path d="M27 27.0004L48.2132 48.2136M48.2132 27L27 48.2132" stroke="#FAF0E7" stroke-width="8" stroke-linecap="round" />
                        </svg>
                        Виды шрамов, когда запрещено
                    </div>
                    <div class="single__text__scars_0__line__text"><b>Келоидные рубцы — глянцевые, выступающие над поверхностью кожи.</b> Такие шрамы плотные плюс вызывают зуд и больше площади чем рана. Часто они могут становиться больше.</div>
                </div>
            </div>

            <div class="single__text__scars_0__row single__text__scars_0__row--2">
                <div class="container">
                    <div class="single__text__scars_0__info single__text__scars_0__info--3 js-reveal-slideUpDelay" data-delay='100'>
                        <div class="single__text__scars_0_text">
                            <p>Фото ваших шрамов помогут нам понять, к какому виду ваш случай относится. Важен формат живой консультации.</p>

                            <p>Результат осмотра вашего шрама — решение. В случае если все хорошо и можно делать, то реально через план плюс рекомендации: какая забота на этом участке кожи до и после процедуры.</p>
                        </div>

                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__scars_0 btn__x js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>

                    </div>
                    <div class="single__text__scars_0__image single__text__scars_0__image--3 js-reveal-slideUpDelay" data-delay='300'>
                        <img src="images/pages/service/scars/text/4.jpg">
                    </div>
                </div>
            </div>
        </div>


        <div class="single__price js_single_ancor_price">
            <div class="container">
                <div class="single__price__title">Цены</div>
                <div class="price__table">
                    <?php $info = [
                            ['Камуфляж шрамов',['от 7 000 ₽'],''],
                            ['Расходники',['5 000 ₽'],''],
                        ];?>
                    <?php foreach ($info as $item):?>
                    <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                    <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                        <div class="price__tr__inner">
                            <div class="price__td price__td--title"><?= $item[0];?></div>
                            <?php if (count($item[1])>0):?>
                            <div class="price__td price__td--val">
                                <?php for ($i=0; $i < count($item[1]); $i++):?>
                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                <?php endfor;?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="btn__y__wrap js-reveal-slideUp">
                    <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                        <span class="btn__y__inner">Получить консультацию</span>
                    </a>
                </div>
            </div>
        </div>

        <?php include 'parts/components/another__service.php'; ?>

    </section>


    <?php include 'parts/main/footer.php'; ?>

</body>

</html>