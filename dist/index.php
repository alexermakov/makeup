<?php require_once "parts/libs/Mobile_Detect.php";?>
<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Главная</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body class='home'>

    <div class="home__promo">
        <div class="home__promo__image">
            <img src="images/pages/home/bg.jpg">
            <?php $detect = new Mobile_Detect;?>
            <?php if ( !$detect->isMobile() ) :?>
            <div class="home__promo__video">
                <video muted autoplay='true' loop src="video/bg_home.mp4" playsinline></video>
            </div>
            <?php endif;?>
        </div>
        <div class="container">

            <a href="index.php" class="home__promo__logo">
                <img src="images/logo.svg">
            </a>

            <div class="home__promo__menu_btn">
                <button class="btn_menu js_btn_menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>

            <div class="home__promo__menu home__promo__menu--top js-reveal-slideDown">

                <a href="brows.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Брови</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/6.jpg">
                        </div>
                    </div>
                </a>

                <a href="lips.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Губы</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/5.jpg">
                        </div>
                    </div>
                </a>

                <a href="scars.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Рубцы / Шрамы</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/1.jpg">
                        </div>
                    </div>
                </a>

                <a href="vitiligo.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Витилиго</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/7.jpg">
                        </div>
                    </div>
                </a>


            </div>

            <div class="home__promo__info">
                <div class="home__promo__info__inner js-reveal-slideUp">
                    <div class="home__promo__info__title">Пeрманeнтный макияж, который <em>никто нe замeтит</em></div>
                    <button data-fancybox data-src="#js__modal__call" class="js__modal btn_default btn__x btn_home__promo">Получить консультацию</button>
                </div>
            </div>


            <div class="home__promo__menu home__promo__menu--bottom js-reveal-slideUp">

                <a href="areola.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Ареола</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/2.jpg">
                        </div>
                    </div>
                </a>
                <a href="laser.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Лазерная эпиляция</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/4.jpg">
                        </div>
                    </div>
                </a>

                <a href="removal.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Удаление</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/8.jpg">
                        </div>
                    </div>
                </a>

                <a href="lamination.php" class="home__promo__menu__item">
                    <div class="home__promo__menu__item__title">Ламинирование</div>
                    <div class="home__promo__menu__item__image">
                        <div class="home__promo__menu__item__image__inner">
                            <img src="images/pages/home/menu/3.jpg">
                        </div>
                    </div>
                </a>
            </div>

            <div class="home__promo__social ">

                <div class="home__promo__social__inner js-reveal-slideRight">
                    <a href="" target='_blank'>
                        <svg width="24" height="17" viewBox="0 0 24 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.299 2.61783C23.1636 2.11566 22.8991 1.65769 22.5318 1.28951C22.1644 0.921334 21.7071 0.655784 21.2052 0.519296C19.3443 0.00832688 11.8996 9.02692e-06 11.8996 9.02692e-06C11.8996 9.02692e-06 4.45613 -0.00830931 2.59406 0.480082C2.09251 0.622852 1.63608 0.892157 1.26859 1.26214C0.901101 1.63213 0.634893 2.09037 0.495523 2.59288C0.00475516 4.45376 1.74104e-06 8.31335 1.74104e-06 8.31335C1.74104e-06 8.31335 -0.00475144 12.192 0.482451 14.0338C0.75576 15.0522 1.55786 15.8567 2.57742 16.1312C4.45731 16.6421 11.8818 16.6505 11.8818 16.6505C11.8818 16.6505 19.3265 16.6588 21.1874 16.1716C21.6894 16.0353 22.1472 15.7704 22.5155 15.403C22.8837 15.0355 23.1497 14.5783 23.2871 14.0766C23.7791 12.2169 23.7826 8.35851 23.7826 8.35851C23.7826 8.35851 23.8064 4.47871 23.299 2.61783ZM9.51828 11.8889L9.52422 4.75915L15.7117 8.32999L9.51828 11.8889Z" fill="white" />
                        </svg>
                    </a>

                    <a href="" target='_blank'>
                        <svg width="24" height="15" viewBox="0 0 24 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M23.2657 1.60816C23.4321 1.0556 23.2657 0.650391 22.4791 0.650391H19.8731C19.2101 0.650391 18.907 1.00094 18.7407 1.38595C18.7407 1.38595 17.4157 4.61693 15.5382 6.71191C14.9322 7.3215 14.6553 7.51401 14.325 7.51401C14.1598 7.51401 13.9197 7.3215 13.9197 6.76895V1.60816C13.9197 0.945089 13.7284 0.650391 13.1759 0.650391H9.08099C8.66746 0.650391 8.41791 0.956972 8.41791 1.24929C8.41791 1.87672 9.35667 2.02169 9.45292 3.78988V7.62571C9.45292 8.46584 9.30201 8.61913 8.96929 8.61913C8.08638 8.61913 5.93793 5.37626 4.66289 1.66401C4.41453 0.942713 4.1638 0.651579 3.49835 0.651579H0.893601C0.148538 0.651579 0 1.00213 0 1.38714C0 2.07873 0.882907 5.50103 4.1127 10.0273C6.2659 13.118 9.29726 14.7935 12.0589 14.7935C13.7142 14.7935 13.9186 14.4216 13.9186 13.7799V11.4437C13.9186 10.6998 14.0766 10.5501 14.6006 10.5501C14.9856 10.5501 15.6487 10.745 17.1947 12.2339C18.9605 13.9997 19.2528 14.7923 20.2451 14.7923H22.8498C23.5937 14.7923 23.9656 14.4204 23.7517 13.686C23.5176 12.9552 22.6739 11.8917 21.5546 10.6333C20.9462 9.91556 20.0371 9.14317 19.7602 8.75697C19.374 8.25907 19.4857 8.03924 19.7602 7.59719C19.7614 7.59838 22.9354 3.128 23.2657 1.60816Z" fill="white" />
                        </svg>
                    </a>

                    <a href="" target='_blank'>
                        <svg width="12" height="23" viewBox="0 0 12 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.20704 22.18V12.4407H10.4927L10.9811 8.62742H7.20704V6.19853C7.20704 5.09817 7.51362 4.34479 9.09287 4.34479H11.094V0.94506C10.1203 0.840717 9.14164 0.790335 8.16243 0.794146C5.25823 0.794146 3.26426 2.56709 3.26426 5.82184V8.62029H0V12.4335H3.27139V22.18H7.20704Z" fill="white" />
                        </svg>
                    </a>
                </div>

            </div>
        </div>
    </div>



    <section class="main__section main__section--home">
        <div class="container">
            <h1 class="title_x title_x__home title_x__home--mobile">Мастер перманентного макияжа</h1>
            <div class="home__info__wrap">
                <div class="home__info js-reveal-slideLeft">
                    <h1 class="title_x title_x__home title_x__home--desktop">Мастер перманентного макияжа</h1>
                    <div class="home__info__about">
                        <div class="home__info__about__name">Кусакина Татьяна</div>
                        <div class="home__info__about__add__info">
                            <b>15 лет</b> опыта и более <b>5000 процедур</b><br>
                            <b>8 лет</b> опыта преподавания и коррекции работ начинающих мастеров
                        </div>
                    </div>
                    <div class="home__info__text">
                        <p>Гостьи студии из <b>более 30 стран</b>, прилетают в Москву, планируя свой визит к Татьяне за 3-4 месяца. Чаще всего прилетают на 2-3 зоны. Их объединяет интеллигентность, зрелость, гармоничность личности и социальный статус. Как одна, все называют причину своего выбора так: <b>«Искали годами, кому доверить свое лицо. Вас порекомендовали близкие»</b>.Все потому что понятие «свой мастер» имеет огромное значение для девушек, которые уже нашли ту самую <b>«лучшую версию себя»</b>, и стремятся ее поддерживать.</p>

                        <p>Большинство мастеров — делают контурные работы, по которым девушки вынуждены затем каждый день наносить макияж, для того чтобы смотрелось хорошо.</p>

                        <p>Татьяна специализируется на своей авторской концепции <b>#БЕЗМАКИЯЖА</b>, суть которой заключается в 100% эволюции в девушку, свободную от нанесения макияжа. Главной уникальностью ее подхода является именно <b>создание натуральных, естественных образов</b>. Исключительно природные цвета и формы. Визитная карточка Татьяны — крылатая фраза: <b>«Перманентный макияж, который заметен, подлежит удалению».</b></p>
                    </div>
                </div>
                <div class="home__image js-reveal-slideRight">
                    <img src="images/__content/pages/home/woman.jpg">
                </div>
                <div class="home__info__btn__wrap">
                    <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x">Получить консультацию</a>
                </div>
            </div>
        </div>
    </section>


    <?php include 'parts/main/footer.php'; ?>

</body>

</html>