<!DOCTYPE html>
<html lang="ru-RU">

<head>
    <title>Лазерная эпиляция</title>
    <?php include 'parts/main/head.php'; ?>
</head>

<body class='page__service'>
    <?php include 'parts/main/header.php'; ?>



    <section class="main__section main__section--service">


        <div class="single__promo">
            <img class='single__promo__bg' src="images/pages/service/laser/1.jpg">
            <div class="container">
                <?= breadcrumbs(['Главная','Лазерная эпиляция']);?>

                <div class="single__promo__wrap">
                    <div class="single__promo__info js-reveal-slideLeft">
                        <h1 class="title_x single__promo__title">Гладкая кожа на каждый день благодаря лазеру <em>LightSheer® DUET™</em></h1>
                        <div class="single__promo__text">
                            <ul>
                                <li>Лазерная эпиляция — это самый современный способ эпиляции.</li>
                                <li>Данный метод подходит любым типам волос с любым оттенком кожи.</li>
                                <li>Если волосы у вас на теле имеют заметный цвет — то вам точно подходит лазерная эпиляция.</li>
                                <li>Лазерная эпиляция — это избавление от постоянного роста волос, за счёт разрушения луковиц.</li>
                                <li>Гладкое лицо и тело, без волос.</li>
                            </ul>
                        </div>
                        <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x btn__y--service js__modal">
                            <span class="btn__y__inner">Получить консультацию</span>
                        </a>
                    </div>

                    <div class="single__promo__image js-reveal-slideRight">
                        <div class="single__promo__image__inner">
                            <img src="images/pages/service/laser/2.jpg" class="single__promo__image__bg">
                            <div class="single__promo__image__info">
                                <div class="single__promo__image__icon">
                                    <img src="images/pages/service/laser/3.svg">
                                </div>
                                <div class="single__promo__image__text">Лазерная эпиляция</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="single__tags js_single__tags js-reveal-slideUp">
            <div class="container">
                <a href=".js_single_ancor_text_1">В каких случаях стоит сделать эпиляцию</a>
                <a href=".js_single_ancor_text_2">Какие будут результаты</a>
                <a href=".js_single_ancor_text_3">От чего зависят результаты</a>
                <a href=".js_single_ancor_result">Результаты наших клиентов</a>
                <a href=".js_single_ancor_histroy">Истории</a>
                <a href=".js_single_ancor_text_4">Почему, выбирают наш лазер</a>
                <a href=".js_single_ancor_text_5">LightSheer® DUET™</a>
                <a href=".js_single_ancor_text_6">Цель лазерной эпиляции</a>
                <a href=".js_single_ancor_text_7">Противопоказания к лазерной эпиляции</a>
                <a href=".js_single_ancor_price">Цены</a>
            </div>
        </div>

        <div class="js_single_ancor_text_1 single__areola__text_1">
            <div class="container">
                <div class="single__areola__text_1__title  js-reveal-slideUpDelay" data-delay='100'>В каких случаях стоит сделать эпиляцию</div>
                <div class="single__areola__text_1__row">
                    <div class="single__areola__text_1__image js-reveal-slideUpDelay" data-delay='100'>
                        <div class="single__areola__text_1__image__inner">
                            <img src="images/pages/service/laser/text/1.jpg" alt="">
                        </div>
                    </div>
                    <div class="single__areola__text_1__info js-reveal-slideUpDelay" data-delay='300'>
                        <p>Если:</p>
                        <ul>
                            <li>У вас интенсивный рост волос на теле.</li>
                            <li>У вас врастают волосы и от них остаются пятна на коже.</li>
                            <li>У вас аллергия на сахарные и восковые крема для депиляции.</li>
                            <li>У вас низкий болевой порог и плохая плотность кожи, часто есть надрывы после депиляции сахаром и воском.</li>
                            <li>Вы хотите гладкое тело 24/7.</li>
                            <li>Вы осознали, что волосы удерживают запахи.</li>
                            <li>Так как вы часто на виду, для вас важны высокие стандарты внешнего вида.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="js_single_ancor_text_2 single__areola__text_2">
            <div class="container">
                <div class="single__areola__info_2 js-reveal-slideUpDelay" data-delay='100'>
                    <div class="single__areola__title_2">Какие будут результаты</div>
                    <div class="single__areola__text_2__inner">
                        <p>Полный курс эпиляции приводит к исчезновению 80-90% волос в желаемой зоне.</p>

                        <p><b>Количество повторений для оптимального результата:</b> <br>
                            Не гормональные зоны:Ноги, руки, спина, живот, поясница <nobr>6-10</nobr> посещений.</p>

                        <p><b>Гормональные зоны:</b> <br> Лицо, грудь, бикини — <nobr>10-15</nobr> посещений.</p>

                        <p>Чем темнее волосы, тем меньше потребуется посещений.</p>
                    </div>
                </div>
                <div class="single__areola__image_2 js-reveal-slideUpDelay" data-delay='300'>
                    <img src="images/pages/service/laser/text/2.jpg" alt="">
                </div>
            </div>
        </div>

        <div class="js_single_ancor_text_3 single__areola__text_3">
            <div class="container">
                <div class="single__areola__text_3__title js-reveal-slideUpDelay" data-delay='100'>От чего зависят результаты</div>
                <div class="single__areola__text_3__text js-reveal-slideUpDelay" data-delay='300'>
                    <p>Результат после прохождения полного курса лазерной эпиляции сохраняется на срок от 10 до 25 лет. <br> У некоторых он сохранится на всю жизнь.</p>

                    <p>Давайте поймем от чего это зависит, и избавимся от ложных ожиданий:</p>

                    <p>Лазерная эпиляция не удаляет: пушковые, седые и светлые волосы. <br>В луковице таких волос нет темного пигмента, поэтому лазер их практически не видит как мишень.</p>

                    <p>Организм человека и химические процессы происходящие в нем — до сих пор до конца не изучен. Нет никаких гарантий, что пройдя полный курс у вас не случится гормональный всплеск (причиной которого могут стать беременность, проблемы с щитовидной железой, сбои иммунитета).</p>

                    <p>Также неизвестно, будет ли ваш образ жизни таким же как сейчас — всегда. Возможно какая-то терапия или прием витаминов, повлечет за собой повторный рост волос. Поэтому утверждать что, что волосы не отрастут никогда, ни один профессионал права не имеет.</p>
                </div>
            </div>
        </div>


        <div class="single__results js_single_ancor_result js-reveal-slideUp">
            <div class="container">
                <div class="single__results__title">Результаты наших клиентов</div>
                <div class="single__results__btns js_single__results__btns">
                    <a href="" class='active'>Галерея</a>
                    <a href="">Видео</a>
                </div>
                <div class="single__results__list js_single__results__list">
                    <div class="single__results__block active">
                        <div class="single__results__slider js_single__results__slider">
                            <?php for ($i=0; $i < 10; $i++):?>
                            <div class="single__results__slide">
                                <div class="single__results__item">
                                    <div class="single__results__item__col">
                                        <div class="single__results__item__col__image">
                                            <img src="images/pages/service/laser/result/<?= $i + 1;?>-0.jpg">
                                        </div>
                                        <div class="single__results__item__col__title single__results__item__col__title--before">До</div>
                                    </div>
                                    <div class="single__results__item__col">
                                        <div class="single__results__item__col__image">
                                            <img src="images/pages/service/laser/result/<?= $i + 1;?>-1.jpg">
                                        </div>
                                        <div class="single__results__item__col__title single__results__item__col__title--after">После</div>
                                    </div>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>
                    </div>
                    <div class="single__results__block">
                        <div class="single__results__slider single__results__slider--video js_single__results__slider__video">
                            <?php for ($i=0; $i < 7; $i++):?>
                            <div class="single__results__slide">
                                <div class="single__results__item">
                                    <a data-fancybox href="https://www.youtube.com/watch?v=z2X2HaTvkl8" class="single__results__item__video">
                                        <img src="images/pages/service/lips/video/1.jpg">
                                    </a>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="single__history js_single_ancor_histroy js-reveal-slideUp">
            <div class="container">
                <div class="single__history__top">
                    <div class="single__history__title">Истории</div>
                    <div class="single__history__arrows js_single__history__arrows">
                        <button class='single__history__arrow'>
                            <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 1L1 8.5L9 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                        <button class='single__history__arrow'>
                            <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L9 8.5L1 16" stroke="#534859" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </button>
                    </div>
                </div>

                <div class="single__history__list js_single__history__list">

                    <?php for ($i=0; $i < 5; $i++):?>
                    <div class="single__history__item__wrap">
                        <div class="single__history__item">
                            <div class="single__history__images">
                                <div class="single__history__image">
                                    <img src="images/pages/service/laser/history/<?= $i % 3 +1;?>-0.jpg">
                                    <div class="single__history__image__text">До</div>
                                </div>
                                <div class="single__history__image">
                                    <img src="images/pages/service/laser/history/<?= $i % 3 +1;?>-1.jpg">
                                    <div class="single__history__image__text">После</div>
                                </div>
                            </div>
                            <div class="single__history__info">
                                <div class="single__history__info__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
                                <div class="single__history__info__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mauris cras ultrices ut sed nec nec, lacus, in. In tellus ut eget leo elit. Neque diam dictum quis et vehicula sit leo fermentum imperdiet.</div>
                                <div class="single__history__info__who">
                                    <div class="single__history__info__who__image">
                                        <img src="images/__content/about.jpg">
                                    </div>
                                    <div class="single__history__info__who__title">
                                        <div class="single__history__info__who__value">Мастер:</div>
                                        <div class="single__history__info__who__name">Имя Фамилия</div>
                                    </div>
                                </div>

                                <a data-fancybox data-src="#js__modal__more" class="single__history__more js__modal btn__default">Читать далее</a>
                            </div>
                        </div>
                    </div>
                    <?php endfor;?>
                </div>
            </div>
        </div>


        <div class="laser__t_block_0">
            <div class="container">
                <div class="laser__t_0__info js_single_ancor_text_4">
                    <div class="laser__t_0__info__title js-reveal-slideUpDelay" data-delay='100'>Почему выбирают наш&nbsp;лазер</div>
                    <div class="laser__t_0__info__text laser__text js-reveal-slideUpDelay" data-delay='100'>
                        <p>LightSheer® DUET™ — это лазер премиум-класса. В нашей комплектации, стоимость такого лазера = 6,9 млн рублей. Работать на нем себе могут позволить только те, кто делает упор на качество услуг.</p>

                        <p>Единственный в мире лазер, в рукоятку которого встроена технология вакуумного усиления. Эта уникальная технология гарантирует высший уровень комфорта и скорости процедур лазерной эпиляции.</p>

                        <p>Он имеет длину волны 800 нанометров и предназначен для проведения процедур эпиляции у пациентов с любым цветом кожи.</p>
                    </div>
                </div>
                <div class="laser__t_0__slider js-reveal-slideUpDelay" data-delay='300'>
                    <?php $slides=[3,4,5,4,3];?>

                    <div class="laser__t_0__slider__main js_laser__t_0__slider__main">
                        <?php foreach ($slides as $slide):?>
                        <div class="laser__t_0__slider__main__item">
                            <div class="laser__t_0__slider__main__item__inner">
                                <img src="images/pages/service/laser/text/<?= $slide;?>.jpg" alt="">
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>

                    <div class="laser__t_0__slider__add js_laser__t_0__slider__add">
                        <?php foreach ($slides as $slide):?>
                        <div class="laser__t_0__slider__add__item">
                            <div class="laser__t_0__slider__add__item__inner">
                                <img src="images/pages/service/laser/text/<?= $slide;?>.jpg" alt="">
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>

                </div>
            </div>
        </div>

        <div class="laser__text__block">
            <div class="container">
                <div class="laser__text js-reveal-slideUpDelay" data-delay='0'>
                    <p>Методики сбривания, выщипывания, применения воска — это всего лишь разрушение стержня волоса, поэтому это дает лишь временный эффект.</p>

                    <p>Именно световые методики удаления волос, к которым относится лазерная и фотоэпиляция, а также уже устаревшая из-за своей трудоемкости электроэпиляция, позволяют добиться перманентного эффекта удаления волос (прекращение роста волос на длительное время (6-12 недель)</p>

                    <p>Лазерное излучение видимой и ближней инфракрасной части спектра хорошо разогревает меланин, поэтому часто используется для эпиляции. Но, поскольку меланин есть не только в стержнях волос, но и в большом количестве в эпидермисе, необходимо решить проблему доставки лазерного излучения на глубину залегания волоса без риска перегрева эпидермиса. Для этого используется подбор длины волны излучения: способность меланина поглощать лазерное излучение уменьшается с увеличением длины волны этого излучения. Ближе к коротковолновой области спектра меланин настолько хорошо поглощает свет, что сильному нагреву подвергается эпидермис даже с минимальным содержанием меланина.<br>Поэтому, например, рубиновый лазер 694 нм может безопасно применяться только на абсолютно белой коже.</p>

                    <p>Распространенный александритовый лазер (755 нанометров) успешно используется для эпиляции вот уже много лет, но практически не применяется для работы на смуглой и черной коже, поскольку так же очень хорошо нагревает меланин. Диодные лазеры 800 нм на сегодняшний день считаются оптимальными по соотношению эффективность / безопасность, поскольку излучение такой длины волны в меньшей степени нагревает эпидермис, и можно безопасно проводить эпиляцию на коже любого фототипа. По многочисленным научным данным диодные лазеры, к которым относится LightSheer® DUET™, показывают максимальную эффективность на коже всех фототипов.</p>

                    <div class="laser__text__image__full__wrap" data-delay='0'>
                        <img src="images/pages/service/laser/text/6.png" alt="" class="laser__text__image__full js_single_ancor_text_5">
                    </div>

                    <p>LightSheer® DUET™ и принцип селективного фототермолиза. Известно, что лазерная энергия определенной длины волны проникает в ткани и поглощается хромофорами. Биологические объекты в тканях, содержащие большое количество хромофоров, Например, сосуды с гемоглобином, а также волосы, окрашенные меланином — нагреваются в процессе поглощения лазерной энергии. Нагрев до температур выше 65 градусов приводит к разрушению этих объектов. Важно, что окружающие ткани, которые содержат мало хромофоров, не поглощают лазерную энергию, а значит не повреждаются.</p>

                    <p>Кожа содержит три основных хромофора: меланин, который присутствует в основном в эпидермисе и волосах: гемоглобин, который присутствует в сосудах дермы; и воду, которая присутствует повсеместно и составляет примерно 70% вещества кожи. Лазерное излучение с длиной волны 800 нм хорошо поглощается меланином, гораздо слабее поглощается гемоглобином и практически не поглощается водой. Поэтому, воздействуя таким излучением на кожу, можно избирательно нагревать и разрушать волосяные фолликулы.</p>

                    <div class="laser__text__block__row">
                        <div class="laser__text__block__row__info js-reveal-slideUpDelay" data-delay='0'>
                            <p>Микрофотография волосяного фолликула до и после обработки LightSheer® DUET™.</p>

                            <p>На фотографии До (слева) отмечена область, содержащая стволовые клетки, на фотографии После (справа) стволовые клетки разрушены. Настоящая эпиляция заключается не только в «сжигании» самого волоса, но и подразумевает разрушение стволовых клеток волосяного фолликула. Цель такого разрушения — предотвратить последующий повторный рост волоса.</p>
                        </div>
                        <div class="laser__text__block__row__image js-reveal-slideUpDelay" data-delay='300'>
                            <img src="images/pages/service/laser/text/7.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="laser__text__block_2 js_single_ancor_text_6 js-reveal-slideUpDelay" data-delay='0'>
            <div class="container">
                <div class="laser__text__block_2__title">Цель лазерной эпиляции</div>
                <div class="laser__text">
                    <p>Это разрушение стволовых клеток волосяного фолликула, при этом такое разрушение возможно только для фолликулов, волос которых находится в стадии роста — анагене. Именно в анагене волос содержит максимальное количество меланина, а, значит, будет хорошо нагреваться. В анагене стволовые клетки фолликула активно делятся и чувствительны к любому агрессивному воздействию.</p>

                    <p>Стволовые клетки волосяных фолликулов, которые находятся не в стадии роста, а в стадии телогена и катагена, не активны, поэтому мало подвержены влиянию нагрева. Из этого следует, что разрушению в ходе лазерной эпиляции подвергаются только те фолликулы, которые находятся в стадии роста. Поскольку на одном и том же участке кожи присутствуют волосы во всех трех стадиях, а процедура эпиляции воздействует только на волосы в стадии анагена, невозможно достигнуть эффекта полного удаления волос всего за одну процедуру.</p>
                </div>
            </div>
        </div>

        <div class="laser__text__block_3 js_single_ancor_text_7">
            <div class="container">
                <div class="laser__text__block_3__title js-reveal-slideUpDelay" data-delay='0'>Противопоказания к&nbsp;лазерной эпиляции</div>
                <div class="laser__text__block_3__info js-reveal-slideUpDelay" data-delay='100'>
                    <div class="laser__text__block_3__text">
                        <p><b>Абсолютные противопоказания:</b></p>
                        <ul>
                            <li>Сахарный диабет</li>
                            <li>Онкологические заболевания, любые опухоли</li>
                            <li>Герпес в острых формах</li>
                            <li>Индивидуальная непереносимость света и тепла</li>
                            <li>Свежий загар</li>
                            <li>Светлые, белые и седые волосы</li>
                        </ul>

                        <p><b>Противопоказания:</b></p>
                        <ul>
                            <li>Острые и хронические заболевания эпидермиса</li>
                            <li>На участке есть родинки</li>
                            <li>Грипп простуда орз</li>
                            <li>Лактация и беременность</li>
                            <li>Склонность к образованию рубцов</li>
                            <li>Царапины, ссадины, синяки на участке</li>
                        </ul>
                    </div>
                </div>
                <div class="laser__text__block_3__image js-reveal-slideUpDelay" data-delay='300'>
                    <img src="images/pages/service/laser/text/8.jpg">
                </div>
            </div>
        </div>

        <div class="laser__text__decore">
            <div class="container">
                <div class="laser__text__decore__inner js-reveal-slideUp">Прежде чем начать курс эпиляции, мы рекомендуем проконсультироваться с эндокринологом и наверняка знать, что у вас в полном порядке гормональный фон, это залог результативной эпиляции.</div>

                <div class="btn__y__wrap js-reveal-slideUp">
                    <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                        <span class="btn__y__inner">Получить консультацию</span>
                    </a>
                </div>
            </div>
        </div>


        <div class="single__price js_single_ancor_price">
            <div class="container">
                <div class="single__price__title">Цены</div>
                <div class="price__table">
                    <?php $info = [
                            ['Комплексы женские',[],'action'],
                            ['Глубокое бикини + подмышки',['4 000 ₽'],''],
                            ['Глубокое бикини + подмышки + голени с коленом ',['6 000 ₽'],''],
                            ['Глубокое бикини + подмышки + ноги полностью ',['8 000 ₽'],''],
                            ['Глубокое бикини + подмышки + ноги полностью + руки полностью',['10 000 ₽'],''],
                            ['Лицо полностью ',['3 500 ₽'],''],
                            ['Эпиляция отдельных зон для женщин',[],'action'],
                            ['Верхняя губа',['1 500 ₽'],''],
                            ['Подбородок',['2 000 ₽'],''],
                            ['Виски/лоб/нос ',['1 500 ₽'],''],
                            ['Шея',['3 000 ₽'],''],
                            ['Руки до локтя',['2 300 ₽'],''],
                            ['Руки выше локтя',['2 500 ₽'],''],
                            ['Руки полностью ',['3 500 ₽'],''],
                            ['Кисти рук с пальцами ',['2 500 ₽'],''],
                            ['Пальцы рук',['1 500 ₽'],''],
                            ['Плечи',['2 500 ₽'],''],
                            ['Линия живота',['1 500 ₽'],''],
                            ['Ареолы',['1 500 ₽'],''],
                            ['Грудь полностью ',['3 500 ₽'],''],
                            ['Декольте',['2 500 ₽'],''],
                            ['Живот полностью',['3 500 ₽'],''],
                            ['Спина нижняя треть ',['3 500 ₽'],''],
                            ['Спина полностью',['5 000 ₽'],''],
                            ['Глубокое бикини',['3 500 ₽'],''],
                            ['Бикини классическое ',['2 500 ₽'],''],
                            ['Межягодичная зона',['1 500 ₽'],''],
                            ['Ягодицы',['2 500 ₽'],''],
                            ['Голени с коленом ',['4 500 ₽'],''],
                            ['Ноги полностью ',['6 000 ₽'],''],
                            ['Бедра передняя/задняя часть',['3 000 ₽'],''],
                            ['Бедра полностью',['4 500 ₽'],''],
                            ['Колени',['1 500 ₽'],''],
                            ['Подъем ног',['1 500 ₽'],''],
                            ['Подъем ног с пальцами',['2 500 ₽'],''],
                            ['Пальцы ног ',['1 000 ₽'],''],
                            ['Мужская эпиляция',[],'action'],
                            ['Лицо полностью',['6 000 ₽'],''],
                            ['Верхняя губа ',['1 500 ₽'],''],
                            ['Щеки',['2 000 ₽'],''],
                            ['Подбородок',['2 000 ₽'],''],
                            ['Бакенбарды',['2 000 ₽'],''],
                            ['Ужиные раковины',['1 000 ₽'],''],
                            ['Межбровье',['1 000 ₽'],''],
                            ['Шея',['3 000 ₽'],''],
                            ['Спина',['5 000 ₽'],''],
                            ['Грудь',['5 000 ₽'],''],
                            ['Плечи',['2 500 ₽'],''],
                            ['Подмышки',['2 000 ₽'],''],
                            ['Живот',['4 000 ₽'],''],
                            ['Поясница',['3 000 ₽'],''],
                            ['Руки до локтя',['3 000 ₽'],''],
                            ['Руки полностью ',['5 000 ₽'],''],
                            ['Бикини классическое',['5 000 ₽'],''],
                            ['Голени',['5 000 ₽'],''],
                            ['Бедра',['5 000 ₽'],''],
                            ['Ноги полностью ',['8 000 ₽'],''],

                        ];?>
                    <?php foreach ($info as $item):?>
                    <?php $addClass='';
                                if ($item[2]== 'title'){
                                    $addClass = ' price__tr__title';
                                };
                                if ($item[2]== 'action'){
                                    $addClass = ' price__tr__action';
                                }
                            ?>
                    <div class="js-reveal-slideUp price__tr<?= $addClass;?>">
                        <div class="price__tr__inner">
                            <div class="price__td price__td--title"><?= $item[0];?></div>
                            <?php if (count($item[1])>0):?>
                            <div class="price__td price__td--val">
                                <?php for ($i=0; $i < count($item[1]); $i++):?>
                                <?php if ($i !=1) {
                                                    echo '<span>'.$item[1][$i].'</span>';
                                                }else{
                                                    echo '<span class="price__td__price--old">'.$item[1][$i].'</span>';
                                                }
                                                ?>
                                <?php endfor;?>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="js-reveal-slideUp btn__y__wrap">
                    <a data-fancybox data-src="#js__modal__call" class="btn__default btn__x js__modal">
                        <span class="btn__y__inner">Получить консультацию</span>
                    </a>
                </div>
            </div>
        </div>



        <?php include 'parts/components/another__service.php'; ?>

    </section>


    <?php include 'parts/main/footer.php'; ?>

</body>

</html>